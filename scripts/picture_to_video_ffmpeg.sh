#!/bin/bash

ffmpeg.exe -f image2 -framerate 25 -pattern_type sequence -start_number 00000 -r 3 -i irc_forward%05d.png -s 720x480 irc_path.avi

ffmpeg.exe -f image2 -framerate 25 -pattern_type sequence -start_number 00001 -r 3 -i SortedClip%05d.png -s 720x480 irc_path.avi

ffmpeg -f image2 -framerate 25 -pattern_type sequence -start_number 00001 -r 3 -i SortedClip%05d.png -s 720x480 irc_path.avi

ffmpeg -f image2 -framerate 27 -pattern_type sequence -start_number 00001 -r 3 -i SortedClip%05d.png -s 1920x1080 irc_path.mp4

ffmpeg -f image2 -framerate 5 -pattern_type sequence -start_number 00001 -i SortedClip%05d.png -s 1920x1080 irc_path.mp4
