"""
parse pattern from check file or output file
"""

import os
import sys
import time
import copy
import re
from shutil import copy2
import itertools
import numpy as np
import read_write_configuration as rwc
import global_settings
import tools
import miller_projection as mp
import reaction_path as rp


def parse_number_atom_atomic_weight_input_log(data_dir, fn1=None):
    """
    parse number of atoms from input.log
    """
    if fn1 is None:
        fn1 = "input.log"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^\s+NAtoms=\s+(\d+)\s+"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match_indicator = re.search(pattern1, line)
        if match_indicator:
            num_atoms = int(match_indicator.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse input.log, number of atoms is ZERO!!!")

    atomic_weight = []
    pattern2 = r"^\s+AtmWgt=\s+((?:[-+]?[0-9]*\.?[0-9]+\s*)+)$"
    for line in lines_data:
        # Regex applied to each line
        match_indicator = re.search(pattern2, line)
        if match_indicator:
            weight_str = str(match_indicator.group(1))
            weight_str_vec = weight_str.split()
            for x in weight_str_vec:
                atomic_weight.append(float(x))
            break
    print(num_atoms, atomic_weight)
    return num_atoms, atomic_weight


def parse_atom_vector_input_log(data_dir, fn1=None):
    """
    parse atom vector from input.log
    """
    if fn1 is None:
        fn1 = "input.log"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    num_atoms, atomic_weight = parse_number_atom_atomic_weight_input_log(
        data_dir, fn1)
    pattern1 = r"^\s+Charge\s+=\s+[-+]?[0-9]*\.?[0-9]+\s+Multiplicity\s+=\s+[-+]?[0-9]" + \
        r"*\.?[0-9]+\s*$"

    atom_vector = []
    line_idx = 0
    atom_indicator = False
    while line_idx < len(lines_data):
        line = lines_data[line_idx]

        if atom_indicator is True:
            line_d = line.split()
            atom_vector.append(line_d[0])
            if len(atom_vector) >= num_atoms:
                break

        # Regex applied to each line
        match_indicator = re.search(pattern1, line)
        if match_indicator:
            atom_indicator = True
            line_idx += 1
            continue

        line_idx += 1
    print(atom_vector)
    return num_atoms, atom_vector, atomic_weight


def parse_total_energy_fchk(data_dir, fn1=None):
    """
    parse total energy from fchk file
    """
    if fn1 is None:
        fn1 = "check_point_file.fchk"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"\s*Total\s+Energy\s+R\s+(-?\d\.\d+E\+?-?\d+)\s*$"
    total_energy_vec = []
    for line in lines_data:
        # Regex applied to each line
        match = re.search(pattern1, line)
        if match:
            total_energy_vec.append(float(match.group(1)))
            break

    print(total_energy_vec)

    if len(total_energy_vec) <= 0:
        raise ValueError(
            "Total energy not found or regular expression is wrong!!!")

    return total_energy_vec


def parse_IRC_TS_coordinate_input_log(data_dir, fn1=None, fn2=None, s2f=False):
    """
    parse Transition State (TS) coordinate from IRC calculation from input.log file
    int unit of Angstrom
    """
    if fn1 is None:
        fn1 = "input.log"
    if fn2 is None:
        fn2 = "IRC_TS_coordinates.csv"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^\s+NAtoms=\s+(\d+)\s+"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match_indicator = re.search(pattern1, line)
        if match_indicator:
            num_atoms = int(match_indicator.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse input.log, number of atoms is ZERO!!!")

    pattern_TS_indicator1 = r"Start new reaction path calculation"
    pattern_TS_indicator2 = r"Current Structure is TS"
    TS_indicator1 = False
    TS_indicator2 = False
    TS_line_idx_max = 0
    while TS_line_idx_max < len(lines_data):
        line = lines_data[TS_line_idx_max]

        if TS_indicator1 is False:
            match_IRC1 = re.search(pattern_TS_indicator1, line)
            if match_IRC1:
                TS_indicator1 = True
            TS_line_idx_max += 1
            continue
        # up to now, IRC_indicator1 shall be True
        elif TS_indicator2 is False:
            match_IRC2 = re.search(pattern_TS_indicator2, line)
            if match_IRC2:
                TS_indicator2 = True
            TS_line_idx_max += 1
            # find proof of TS, the information before this line should belongs to TS
            break
    print("TS line number max: {:d}".format(TS_line_idx_max))

    pattern2 = r"^.+Coordinates \(Angstroms\)\s*"
    # pattern3 = r"^\s+(\d+)\s+(\d+\.?\d?)\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)" + \
    #     r"\s+([-+]?[0-9]*\.?[0-9]+)\s*$"
    pattern3 = r"^\s+(\d+)\s+(\d+\.?\d?)\s+(?:\d+\s+)*([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*" + \
        r"\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s*$"

    coor_vec = []
    dof = num_atoms * 3
    dof_counter = 0
    coordinate_indicator = False
    line_idx = 0
    line_gap = 3
    while line_idx <= TS_line_idx_max:
        line = lines_data[line_idx]

        if coordinate_indicator is True:
            # parse for cartesian coordinates
            match_pos = re.search(pattern3, line)
            if not match_pos:
                raise ValueError(
                    "{} lines after pattern {}, no match found!!!".format(line_gap, pattern2))
            else:
                coor_vec.append(float(match_pos.group(3)))
                coor_vec.append(float(match_pos.group(4)))
                coor_vec.append(float(match_pos.group(5)))
                dof_counter += 3

                if dof_counter >= dof:
                    # visited all degree of freedom, restart a potential cycle
                    coordinate_indicator = False
                    dof_counter = 0
                    continue

        if coordinate_indicator is False:
            # Regex applied to each line
            match_indicator = re.search(pattern2, line)
            if match_indicator:
                coordinate_indicator = True
                line_idx += line_gap
                continue

        line_idx += 1

    coor_mat = np.reshape(coor_vec, (-1, dof))
    print(coor_mat)

    # save csv file
    if s2f is True:
        with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
            for idx in range(np.shape(coor_mat)[0]):
                for idx2, val2 in enumerate(coor_mat[idx]):
                    if idx2 == 0:
                        f_hanlder2.write("{0:1.8E}".format(float(val2)))
                    else:
                        f_hanlder2.write(',' + "{0:1.8E}".format(float(val2)))
                f_hanlder2.write('\n')

    return np.ravel(coor_mat[-1])


def parse_IRC_reaction_coordinate_input_log(data_dir, fn1=None, fn2=None, s2f=False,
                                            reorder_idx=None):
    """
    parse reaction coordinate from IRC calculation from input.log file
    int unit of Angstrom
    """
    if fn1 is None:
        fn1 = "input.log"
    if fn2 is None:
        fn2 = "IRC_coordinates.csv"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^\s+NAtoms=\s+(\d+)\s+"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match_indicator = re.search(pattern1, line)
        if match_indicator:
            num_atoms = int(match_indicator.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse input.log, number of atoms is ZERO!!!")

    pattern2 = r"^\s+CURRENT STRUCTURE"
    # pattern3 = r"^\s+(\d+)\s+(\d+\.?\d?)\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+"+\
    #     r"([-+]?[0-9]*\.?[0-9]+)\s*$"
    pattern3 = r"^\s+(\d+)\s+(\d+\.?\d?)\s+(?:\d+\s+)*([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]" +\
        r"*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s*$"

    coor_vec = []
    dof = num_atoms * 3
    dof_counter = 0
    coordinate_indicator = False
    line_idx = 0
    line_gap = 6
    while line_idx < len(lines_data):
        line = lines_data[line_idx]

        if coordinate_indicator is True:
            # parse for cartesian coordinates
            match_pos = re.search(pattern3, line)
            if not match_pos:
                raise ValueError(
                    "{} lines after pattern {}, no match found!!!".format(line_gap, pattern2))
            else:
                coor_vec.append(float(match_pos.group(3)))
                coor_vec.append(float(match_pos.group(4)))
                coor_vec.append(float(match_pos.group(5)))
                dof_counter += 3

                if dof_counter >= dof:
                    # visited all degree of freedom, restart a potential cycle
                    coordinate_indicator = False
                    dof_counter = 0
                    continue

        if coordinate_indicator is False:
            # Regex applied to each line
            match_indicator = re.search(pattern2, line)
            if match_indicator:
                coordinate_indicator = True
                line_idx += line_gap
                continue

        line_idx += 1

    print("Shape before appending TS: ({}, {})".format(
        divmod(len(coor_vec), dof)[0], dof))
    # append TS to the end
    coor_vec.extend(parse_IRC_TS_coordinate_input_log(
        data_dir, fn1, None, s2f))
    print("Shape after appending TS: ({}, {})".format(
        divmod(len(coor_vec), dof)[0], dof))

    coor_mat = np.reshape(coor_vec, (-1, dof))

    # reordered it
    coor_mat_2 = copy.deepcopy(coor_mat)
    if reorder_idx is not None:
        if len(reorder_idx) == len(coor_mat_2):
            for idx, _ in enumerate(coor_mat):
                coor_mat_2[int(reorder_idx[idx])] = coor_mat[idx]

    # save csv file
    if s2f is True:
        with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
            for idx in range(np.shape(coor_mat_2)[0]):
                for idx2, val2 in enumerate(coor_mat_2[idx]):
                    if idx2 == 0:
                        f_hanlder2.write("{0:1.8E}".format(float(val2)))
                    else:
                        f_hanlder2.write(',' + "{0:1.8E}".format(float(val2)))
                f_hanlder2.write('\n')

    return coor_mat_2


def flattened_hessian_to_hessian_matrix(hessian_vec=None):
    """
    convert flattened hessian, presumably a vector to a symmetric matrix
    """
    coeff = [1.0, 1.0, -2*len(hessian_vec)]
    # there must be a positive and a negative root of this quadratic equation, need the positive one
    dof = int(np.max(np.roots(coeff)))
    # print(dof)
    hessian_mat = np.ones((dof, dof))
    counter = 0
    # fill bottom left matrix
    for row_idx in range(dof):
        for col_idx in range(dof):
            if (row_idx >= col_idx):
                hessian_mat[row_idx, col_idx] = hessian_vec[counter]
                counter += 1
    # fill top right matrix since it is symmetric
    for row_idx in range(dof):
        for col_idx in range(dof):
            if (row_idx < col_idx):
                hessian_mat[row_idx, col_idx] = hessian_mat[col_idx, row_idx]

    # print(hessian_mat)
    return hessian_mat


def parse_IRC_TS_hessian_matrix_input_log(data_dir, fn1=None, fn2=None, s2f=False, magic_n_col=5):
    """
    parse Transition State (TS) hessian matrix from IRC calculation from input.log file
    magic number of col (magic_n_col) is number of columns Gaussian output file used
    """
    if fn1 is None:
        fn1 = "input.log"
    if fn2 is None:
        fn2 = "IRC_TS_hessian_matrix.csv"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^\s+NAtoms=\s+(\d+)\s+"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match_indicator = re.search(pattern1, line)
        if match_indicator:
            num_atoms = int(match_indicator.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse input.log, number of atoms is ZERO!!!")

    pattern_TS_indicator1 = r"Start new reaction path calculation"
    pattern_TS_indicator2 = r"Current Structure is TS"
    TS_indicator1 = False
    TS_indicator2 = False
    TS_line_idx_max = 0
    while TS_line_idx_max < len(lines_data):
        line = lines_data[TS_line_idx_max]

        if TS_indicator1 is False:
            match_IRC1 = re.search(pattern_TS_indicator1, line)
            if match_IRC1:
                TS_indicator1 = True
            TS_line_idx_max += 1
            continue
        # up to now, IRC_indicator1 shall be True
        elif TS_indicator2 is False:
            match_IRC2 = re.search(pattern_TS_indicator2, line)
            if match_IRC2:
                TS_indicator2 = True
            TS_line_idx_max += 1
            # find proof of TS, the information before this line should belongs to TS
            break
    print("TS line number max: {:d}".format(TS_line_idx_max))

    pattern2 = r"^\s+Force\s+constants\s+in\s+Cartesian\s+coordinates.+$"
    pattern3 = r"^\s+(\d+)\s+([-+]?[0-9]*\.?[0-9]+D+[-+]?[0-9]+)\s*([-+]?[0-9]*\.?[0-9]+D+" +\
        r"[-+]?[0-9]+)?\s*([-+]?[0-9]*\.?[0-9]+D+[-+]?[0-9]+)?\s*([-+]?[0-9]*\.?[0-9]+D+[-+]?" +\
        r"[0-9]+)?\s*([-+]?[0-9]*\.?[0-9]+D+[-+]?[0-9]+)?\s*$"

    hessian_vec = []
    dof = int(((num_atoms * 3)**2 - (num_atoms*3))/2 + num_atoms*3)
    print(dof)
    dof_counter = 0
    hessian_indicator = False
    line_idx = 0
    line_gap = 2
    # local dict with keys being row numbers
    local_dict = {}
    while line_idx < TS_line_idx_max:
        line = lines_data[line_idx]

        ############################################################################################
        # after this point, IRC_indicator1 and IRC_indicator2 shall both be true
        ############################################################################################
        if hessian_indicator is True:
            # parse for cartesian coordinates
            match_pos = re.search(pattern3, line)
            if match_pos:
                # since the first element is the row number
                local_n_element = match_pos.lastindex - 1

                row_number = str(match_pos.group(1))
                if row_number not in local_dict:
                    local_dict[row_number] = []
                for l_i in range(2, local_n_element+2):
                    local_dict[row_number].append(
                        float(str(match_pos.group(l_i)).replace("D", "E")))

                dof_counter += local_n_element

                if dof_counter >= dof:
                    # visited all degree of freedom, restart a potential cycle
                    # save current cycle to hessian vector
                    if dof_counter == dof:
                        for keys in local_dict:
                            # print(keys, local_dict[keys])
                            for correctly_ordered_val in local_dict[keys]:
                                hessian_vec.append(correctly_ordered_val)
                        # print(len(hessian_vec))
                        # raise ValueError("STOP!!!")

                    hessian_indicator = False
                    dof_counter = 0
                    local_dict = {}
                    continue

        if hessian_indicator is False:
            # Regex applied to each line
            match_indicator = re.search(pattern2, line)
            if match_indicator:
                hessian_indicator = True
                line_idx += line_gap
                continue

        line_idx += 1

    # print(hessian_vec)
    hessian_mat = np.reshape(hessian_vec, (-1, dof))
    print(np.shape(hessian_mat))

    hessian_mat_2 = [np.ravel(flattened_hessian_to_hessian_matrix(
        hessian_mat[idx])) for idx in range(len(hessian_mat))]
    print(np.shape(hessian_mat_2))

    # save csv file
    if s2f is True:
        with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
            for idx in range(np.shape(hessian_mat_2)[0]):
                for idx2, val2 in enumerate(hessian_mat_2[idx]):
                    if idx2 == 0:
                        f_hanlder2.write("{0:1.8E}".format(float(val2)))
                    elif idx2 > 0:
                        f_hanlder2.write(',' + "{0:1.8E}".format(float(val2)))
                f_hanlder2.write('\n')

    return hessian_mat_2


def parse_IRC_hessian_matrix_input_log(data_dir, fn1=None, fn2=None, s2f=False, magic_n_col=5,
                                       reorder_idx=None):
    """
    parse hessian matrix from IRC calculation from input.log file
    magic number of col (magic_n_col) is number of columns Gaussian output file used
    """
    if fn1 is None:
        fn1 = "input.log"
    if fn2 is None:
        fn2 = "IRC_hessian_matrix.csv"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^\s+NAtoms=\s+(\d+)\s+"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match_indicator = re.search(pattern1, line)
        if match_indicator:
            num_atoms = int(match_indicator.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse input.log, number of atoms is ZERO!!!")

    pattern_IRC_indicator1 = r"Start new reaction path calculation"
    pattern_IRC_indicator2 = r"Current Structure is TS"

    pattern2 = r"^\s+Force\s+constants\s+in\s+Cartesian\s+coordinates.+$"
    pattern3 = r"^\s+(\d+)\s+([-+]?[0-9]*\.?[0-9]+D+[-+]?[0-9]+)\s*([-+]?[0-9]*\.?[0-9]+D+[-+]?" +\
        r"[0-9]+)?\s*([-+]?[0-9]*\.?[0-9]+D+[-+]?[0-9]+)?\s*([-+]?[0-9]*\.?[0-9]+D+[-+]?[0-9]+)?" +\
        r"\s*([-+]?[0-9]*\.?[0-9]+D+[-+]?[0-9]+)?\s*$"

    hessian_vec = []
    dof = int(((num_atoms * 3)**2 - (num_atoms*3))/2 + num_atoms*3)
    print(dof)
    dof_counter = 0
    IRC_indicator1 = False
    IRC_indicator2 = False
    hessian_indicator = False
    line_idx = 0
    line_gap = 2
    # local dict with keys being row numbers
    local_dict = {}
    while line_idx < len(lines_data):
        line = lines_data[line_idx]

        if IRC_indicator1 is False:
            match_IRC1 = re.search(pattern_IRC_indicator1, line)
            if match_IRC1:
                IRC_indicator1 = True
            line_idx += 1
            continue
        # up to now, IRC_indicator1 shall be True
        elif IRC_indicator2 is False:
            match_IRC2 = re.search(pattern_IRC_indicator2, line)
            if match_IRC2:
                IRC_indicator2 = True
            line_idx += 1
            continue
        ############################################################################################
        # after this point, IRC_indicator1 and IRC_indicator2 shall both be true
        ############################################################################################
        if hessian_indicator is True:
            # parse for cartesian coordinates
            match_pos = re.search(pattern3, line)
            if match_pos:
                # since the first element is the row number
                local_n_element = match_pos.lastindex - 1

                row_number = str(match_pos.group(1))
                if row_number not in local_dict:
                    local_dict[row_number] = []
                for l_i in range(2, local_n_element+2):
                    local_dict[row_number].append(
                        float(str(match_pos.group(l_i)).replace("D", "E")))

                dof_counter += local_n_element

                if dof_counter >= dof:
                    # visited all degree of freedom, restart a potential cycle
                    # save current cycle to hessian vector
                    if dof_counter == dof:
                        for keys in local_dict:
                            # print(keys, local_dict[keys])
                            for correctly_ordered_val in local_dict[keys]:
                                hessian_vec.append(correctly_ordered_val)
                        # print(len(hessian_vec))
                        # raise ValueError("STOP!!!")

                    hessian_indicator = False
                    dof_counter = 0
                    local_dict = {}
                    continue

        if hessian_indicator is False:
            # Regex applied to each line
            match_indicator = re.search(pattern2, line)
            if match_indicator:
                hessian_indicator = True
                line_idx += line_gap
                continue

        line_idx += 1

    # print(hessian_vec)
    hessian_mat = np.reshape(hessian_vec, (-1, dof))
    hessian_mat_2 = copy.deepcopy(hessian_mat)

    # reordered it
    hessian_mat_2 = copy.deepcopy(hessian_mat_2)
    if reorder_idx is not None:
        if len(reorder_idx) == len(hessian_mat_2):
            for idx, _ in enumerate(hessian_mat):
                hessian_mat_2[int(reorder_idx[idx])] = hessian_mat[idx]

    print(np.shape(hessian_mat_2))
    hessian_mat_3 = [np.ravel(flattened_hessian_to_hessian_matrix(
        hessian_mat_2[idx])) for idx in range(len(hessian_mat_2))]
    print(np.shape(hessian_mat_3))

    print("Shape before appending TS: {}".format(np.shape(hessian_mat_3)))
    hessian_mat_3 = np.vstack((hessian_mat_3, parse_IRC_TS_hessian_matrix_input_log(
        data_dir, fn1, None, s2f, magic_n_col)))
    print("Shape after appending TS: {}".format(np.shape(hessian_mat_3)))

    # save csv file
    if s2f is True:
        with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
            for idx in range(np.shape(hessian_mat_3)[0]):
                for idx2, val2 in enumerate(hessian_mat_3[idx]):
                    if idx2 == 0:
                        f_hanlder2.write("{0:1.8E}".format(float(val2)))
                    elif idx2 > 0:
                        f_hanlder2.write(',' + "{0:1.8E}".format(float(val2)))
                f_hanlder2.write('\n')

    return hessian_mat_3


def parse_IRC_reaction_coordinate_input_log_v2(data_dir, Npoints, fn1=None, fn2=None, s2f=False,
                                               magic_n_col=6, exclude=[0, 1]):
    """
    parse Transition State (TS) hessian matrix from IRC calculation from input.log file
    magic number of col (magic_n_col) is number of columns Gaussian output file used
    """
    if fn1 is None:
        fn1 = "input.log"
    if fn2 is None:
        fn2 = "IRC_TS_hessian_matrix.csv"
    if exclude is None:
        exclude = []
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^\s+NAtoms=\s+(\d+)\s+"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match_indicator = re.search(pattern1, line)
        if match_indicator:
            num_atoms = int(match_indicator.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse input.log, number of atoms is ZERO!!!")

    print("num_atoms:\t", num_atoms)
    pattern_start_indicator1 = r"Summary of reaction path following"
    pattern_end_indicator2 = r"^\s?Total number of Hessian calculations.*$"
    start_indicator1 = False
    end_indicator2 = False
    line_idx_min = 0
    line_idx_max = 0
    while line_idx_max < len(lines_data):
        line = lines_data[line_idx_max]

        if start_indicator1 is False:
            match_1 = re.search(pattern_start_indicator1, line)
            if match_1:
                start_indicator1 = True
                line_idx_min = line_idx_max
            line_idx_max += 1
            continue
        # up to now, start_indicator1 shall be True
        elif end_indicator2 is False:
            match_2 = re.search(pattern_end_indicator2, line)
            if match_2:
                end_indicator2 = True
                # find proof of end, the information before this line should belongs to what wanted
                break
            line_idx_max += 1
    print("Line number min: {:d}".format(line_idx_min))
    print("Line number max: {:d}".format(line_idx_max))

    base_pattern = r"[-+]?[0-9]*\.?[0-9]+D*[-+]?[0-9]*"
    pattern_array = [base_pattern] * magic_n_col
    # match group
    for idx, val in enumerate(pattern_array):
        pattern_array[idx] = r"(" + val + r")"
    pattern3 = r"\s+".join(pattern_array)

    xyz_vec = []
    dof = num_atoms*3
    print("Degree of freedom: {0:4d}".format(dof))
    dof_counter_dict = {}
    line_idx = line_idx_min
    # local dict with keys being row numbers
    local_dict = {}
    while line_idx < line_idx_max:
        line = lines_data[line_idx]
        # after this point, search in the range of (line_idx_min, line_idx_max)
        match_pos = re.search(pattern3, line)
        if match_pos:
                # since the first element is the row number
            local_n_element = match_pos.lastindex - 1
            row_number = str(match_pos.group(1))
            if row_number not in local_dict:
                local_dict[row_number] = []
            for l_i in range(2, local_n_element+2):
                local_dict[row_number].append(
                    float(str(match_pos.group(l_i)).replace("D", "E")))
            if row_number not in dof_counter_dict:
                dof_counter_dict[row_number] = 0
            dof_counter_dict[row_number] += local_n_element

        line_idx += 1

    if Npoints != len(local_dict):
        raise ValueError(
            "NPoints {0:d} should be equal to len(local_dict) {1:d} keyed by Point Number".
            format(Npoints, len(local_dict)))
    # save current cycle to hessian vector
    for row_number in local_dict:
        assert(len(local_dict[row_number]) >= dof)
        for idx, val in enumerate(local_dict[row_number]):
            if idx not in exclude:
                xyz_vec.append(val)
        assert(len(xyz_vec) % dof == 0)

    xyz_mat = np.reshape(xyz_vec, (-1, dof))
    print(np.shape(xyz_mat))

    # save csv file
    if s2f is True:
        with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
            for idx in range(np.shape(xyz_mat)[0]):
                for idx2, val2 in enumerate(xyz_mat[idx]):
                    if idx2 == 0:
                        f_hanlder2.write("{0:1.8E}".format(float(val2)))
                    elif idx2 > 0:
                        f_hanlder2.write(',' + "{0:1.8E}".format(float(val2)))
                f_hanlder2.write('\n')

    return xyz_mat


def parse_atomic_weight(data_dir, g_s, fn1=None):
    """
    parse from either check file or output file, to get the hessian matrix
    """
    if fn1 is None:
        fn1 = "check_point_file.fchk"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^Number of atoms\s+I\s+(\d+)$"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match = re.search(pattern1, line)
        if match:
            num_atoms = int(match.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse check file, number of atoms is ZERO!!!")

    pattern2 = r"^Real atomic weights\s+R\s+N=\s+(\d+)$"

    atomic_weight_mat = []
    atomic_weight_vec = []
    n_mat_ele = 0
    ele_counter = 0
    hessian_indicator = False
    for line in lines_data:
        if hessian_indicator is True:
            # start parse position
            pos_vec = line.split()
            # print(pos_vec)
            for x in pos_vec:
                atomic_weight_vec.append(float(x))
                ele_counter += 1
                if ele_counter >= n_mat_ele:
                    # visited all degree of freedom, restart a potential cycle
                    hessian_indicator = False
                    ele_counter = 0
                    atomic_weight_mat.append(atomic_weight_vec)
                    atomic_weight_vec = []
                    break

        if hessian_indicator is False:
            # Regex applied to each line
            match = re.search(pattern2, line)
            if match:
                hessian_indicator = True
                n_mat_ele = int(match.group(1))
    # print(n_mat_ele)
    print(np.shape(atomic_weight_mat))
    atomic_weight_vec = list(itertools.chain.from_iterable(atomic_weight_mat))[
        0:num_atoms]
    # print(atomic_weight_vec)

    return atomic_weight_vec


def parse_forces_gradients(data_dir, g_s, fn1=None, fn2=None):
    """
    Parses from either check file or output file, to get the gadient, namely forces in unit of
    Hartree/Bohr
    """
    if fn1 is None:
        fn1 = "input.log"
    if fn2 is None:
        fn2 = "forces_gradients.csv"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    # pattern1 = r"^Number of atoms\s+I\s+(\d+)$"
    pattern1 = r"^\s*NAtoms=\s+(\d+).*$"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match = re.search(pattern1, line)
        if match:
            num_atoms = int(match.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse check file, number of atoms is ZERO!!!")

    pattern2 = r"^\s+Center\s+Atomic\s+Forces\s*\(Hartrees\/Bohr\)\s*$"
    pattern3 = r"^\s*\d+\s+\d+\s+(-?\d\.\d+)\s+(-?\d\.\d+)\s+(-?\d\.\d+)\s*$"

    forces_mat = []
    forces_vec = []
    n_mat_ele = num_atoms * 3
    ele_counter = 0
    hessian_indicator = False
    for line_idx in range(len(lines_data)):
        line = lines_data[line_idx]

        if hessian_indicator is True:
            # start parse position
            match = re.search(pattern3, line)
            if match:
                pos_vec = [match.group(1), match.group(2), match.group(3)]
                # print(pos_vec)

                for x in pos_vec:
                    forces_vec.append(float(x))
                    ele_counter += 1
                    if ele_counter >= n_mat_ele:
                        # visited all degree of freedom, restart a potential cycle
                        hessian_indicator = False
                        ele_counter = 0
                        forces_mat.append(forces_vec)
                        forces_vec = []
                        break

        if hessian_indicator is False:
            # Regex applied to each line
            match = re.search(pattern2, line)
            if match:
                hessian_indicator = True
                # skip two lines as soon as we find the beginning
                line_idx += 2
    # print(n_mat_ele)
    print(np.shape(forces_mat))

    # save csv file
    with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
        for _, h_vec in enumerate(forces_mat):
            for idx2, x in enumerate(h_vec):
                if idx2 != 0:
                    f_hanlder2.write(',')
                f_hanlder2.write("{0:1.8E}".format(float(x)))
            f_hanlder2.write('\n')

    return forces_vec


def parse_hessian_matrix(data_dir, g_s, fn1=None, fn2=None):
    """
    parse from either check file or output file, to get the hessian matrix
    """
    if fn1 is None:
        fn1 = "check_point_file.fchk"
    if fn2 is None:
        fn2 = "hessian_matrix.csv"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^Number of atoms\s+I\s+(\d+)$"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match = re.search(pattern1, line)
        if match:
            num_atoms = int(match.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse check file, number of atoms is ZERO!!!")

    pattern2 = r"^Cartesian Force Constants\s+R\s+N=\s+(\d+)$"

    hessian_mat = []
    hessian_vec = []
    n_mat_ele = 0
    ele_counter = 0
    hessian_indicator = False
    for line in lines_data:
        if hessian_indicator is True:
            # start parse position
            pos_vec = line.split()
            # print(pos_vec)
            for x in pos_vec:
                hessian_vec.append(float(x))
                ele_counter += 1
                if ele_counter >= n_mat_ele:
                    # visited all degree of freedom, restart a potential cycle
                    hessian_indicator = False
                    ele_counter = 0
                    hessian_mat.append(hessian_vec)
                    hessian_vec = []
                    break

        if hessian_indicator is False:
            # Regex applied to each line
            match = re.search(pattern2, line)
            if match:
                hessian_indicator = True
                n_mat_ele = int(match.group(1))
    print(n_mat_ele)
    print(np.shape(hessian_mat))

    # save csv file
    with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
        for _, h_vec in enumerate(hessian_mat):
            for idx2, x in enumerate(h_vec):
                if idx2 != 0:
                    f_hanlder2.write(',')
                f_hanlder2.write("{0:1.8E}".format(float(x)))
            f_hanlder2.write('\n')


def parse_optimized_coordinates_xyz(data_dir, g_s, fn1=None, fn2=None, fn3=None,
                                    au_2_anstroms=0.529177249):
    """
    parse from either check file or output file, to get the optimized cartesian coordinates
    Scaling coordinates for geometry "geometry" by  1.889725989
    (inverse scale =  0.529177249)
    output coordinates in angstroms (scale by  1.889725989 to convert to a.u.)
    """
    if fn1 is None:
        fn1 = "check_point_file.fchk"
    if fn2 is None:
        fn2 = "optimized_coordinates.csv"
    if fn3 is None:
        fn3 = "optimized_coordinates.json"
    # read the whole file as a single string
    with open(os.path.join(data_dir, fn1), 'r') as f_hanlder1:
        lines_data = f_hanlder1.readlines()
    pattern1 = r"^Number of atoms\s+I\s+(\d+)$"
    num_atoms = 0
    for line in lines_data:
        # Regex applied to each line
        match = re.search(pattern1, line)
        if match:
            num_atoms = int(match.group(1))
            break
    if num_atoms <= 0:
        raise ValueError("Parse check file, number of atoms is ZERO!!!")

    pattern2 = r"^Current cartesian coordinates\s+R\s+N=\s+(\d+)$"

    coor_vec = []
    dof = 0
    dof_counter = 0
    coordinate_indicator = False
    for line in lines_data:
        if coordinate_indicator is True:
            # start parse position
            pos_vec = line.split()
            # print(pos_vec)
            for x in pos_vec:
                coor_vec.append(float(x))
                dof_counter += 1
                if dof_counter >= dof:
                    # visited all degree of freedom, restart a potential cycle
                    coordinate_indicator = False
                    dof_counter = 0
                    break

        if coordinate_indicator is False:
            # Regex applied to each line
            match = re.search(pattern2, line)
            if match:
                coordinate_indicator = True
                dof = int(match.group(1))
    print(dof)
    # print(coor_vec)
    # convert fomr a.u to angstroms
    for idx, _ in enumerate(coor_vec):
        coor_vec[idx] *= au_2_anstroms
    coor_mat = np.reshape(coor_vec, (-1, 3))
    # print(coor_mat)

    # save csv file
    atom_vec = global_settings.get_atom_vector(g_s)
    with open(os.path.join(data_dir, fn2), 'w') as f_hanlder2:
        for idx in range(np.shape(coor_mat)[0]):
            # print(coor_mat[idx])
            name_idx = idx % len(atom_vec)
            f_hanlder2.write(atom_vec[name_idx])
            for x in coor_mat[idx]:
                f_hanlder2.write(',' + "{0:1.8E}".format(float(x)))
            f_hanlder2.write('\n')

    # save json file
    coor_dict = {}
    coor_dict["geometry"] = []
    for idx in range(np.shape(coor_mat)[0]):
        entry = {}
        name_idx = idx % len(atom_vec)
        entry["atom"] = atom_vec[name_idx]
        entry["xyz"] = list(coor_mat[idx])
        coor_dict["geometry"].append(entry)

    rwc.write_configuration(coor_dict, os.path.join(data_dir, fn3))

    # print(coor_dict)
    return coor_dict


if __name__ == '__main__':
    TIME_I = time.time()

    # source file directory
    SRC_DIR = os.path.abspath(os.path.join(
        os.path.realpath(sys.argv[0]), os.pardir))
    print(SRC_DIR)
    # data directory
    # DATA_DIR = r"E:\Documents\gaussian_files\gaussian_solely" +\
    #     r"\case8_Carbonic_Acid_TS_freq_from_IRC\data_after"
    DATA_DIR = r"/Users/invictus/Documents/data/case20_Carbonic_Acid_TS_IRC" +\
        r"/data_after"
    print(DATA_DIR)

    # read global settings
    # G_S = global_settings.get_setting(DATA_DIR)

    # parse_optimized_coordinates_xyz(DATA_DIR, G_S)
    # parse_hessian_matrix(DATA_DIR, G_S)
    # parse_forces_gradients(DATA_DIR, G_S)
    # parse_total_energy_fchk(DATA_DIR)
    # parse_IRC_reaction_coordinate_input_log(
    #     DATA_DIR, fn1=None, fn2=None, s2f=True)
    parse_IRC_reaction_coordinate_input_log_v2(DATA_DIR, Npoints=51)
    # parse_number_atom_atomic_weight_input_log(DATA_DIR)
    # parse_atom_vector_input_log(DATA_DIR)

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
