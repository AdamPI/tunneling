"""
    curvature related functions
    reaction path is usually referred as s
"""
import numpy as np
import copy
import math
import tools
import os
import time
import sys


def IRC_coordinates_to_reaction_path_s(coor_mat_in=None):
    """
    give a list of reaction coordinates, calculate the reaction path s
    s_i = dist(X_{i}, X_{i-1})
    s_0 = 0
    in unit of angstrom
    """
    if coor_mat_in is None:
        raise ValueError("coordinates is None!!!")
    coor_mat = copy.deepcopy(coor_mat_in)
    for idx, val in enumerate(coor_mat):
        coor_mat[idx] = np.array(val).flatten()
    # print(coor_mat)

    n_row, _ = np.shape(coor_mat)
    # print(n_row)

    s_vec = np.ones(n_row, dtype=float)
    s_vec[0] = 0.0

    # print(s_vec)
    for idx in range(1, n_row):
        coor_vec = np.subtract(coor_mat[idx], coor_mat[idx-1])
        s_vec[idx] = np.linalg.norm(coor_vec, ord=2) + s_vec[idx-1]

    # print(dist_vec, s_vec)
    # print(s_vec)
    return s_vec


def cal_dihedral_angle_from_IRC_path(data_dir, xyz_mat_in=None, atom_list=[1, 2, 3, 4]):
    """
    * calculate dihedral angle from IRC path calculation, notice the input atom_list start from 1 
        instead of 0, because , that's gaussian convention.
    * new_2_old, represents in index from ordered dihedral angles to the gaussian output angle 
        index/relative position
    """
    if atom_list is None or len(atom_list) != 4:
        raise ValueError("atom_list must be a vector of length 4!!!")
    if xyz_mat_in is None:
        raise ValueError("XYZ in cartesian coordinate can not be None!!!")

    xyz_mat_local = copy.deepcopy(xyz_mat_in)

    # print(xyz_mat_local)
    _, cols = np.shape(xyz_mat_local)
    for _, val in enumerate(atom_list):
        if val < 0 or val >= int(cols/3):
            raise ValueError(
                "atom index in atom_list must be > 0 and <=" + str(cols/3)+"!!!")

    di_angles = []
    for _, val in enumerate(xyz_mat_local):
        xyz_mat_local_point = np.reshape(val, (-1, 3))
        # print(xyz_mat_local_point)
        v1 = xyz_mat_local_point[int(atom_list[0])-1]
        v2 = xyz_mat_local_point[int(atom_list[1])-1]
        v3 = xyz_mat_local_point[int(atom_list[2])-1]
        v4 = xyz_mat_local_point[int(atom_list[3])-1]
        di_angles.append(tools.calculate_dihedral_angle(v1, v2, v3, v4))

    # print(di_angles)
    # descending order
    new_2_old = np.argsort(di_angles)[::-1]
    old_2_new = np.ones(len(new_2_old))
    for idx, val in enumerate(new_2_old):
        old_2_new[val] = int(idx)

    sorted_di_angles = copy.deepcopy(di_angles)
    sorted_di_angles = sorted(sorted_di_angles, reverse=True)

    return new_2_old, old_2_new, di_angles, sorted_di_angles


def cal_tangent_curvature_along_reaction_path_s(s_vec=None, coor_mat_in=None):
    """
    Notice here the reaction path shall be treated as a UNITLESS  parameter, thereafter the tangent 
    vector and the curvature vector, namely the 1st order and 2nd order derivative should have unit
     of [X], or says unit of [R]
    Calculate curvature, suppose the cartesian coordinate is a parametric function of s X(s)
    https://en.wikipedia.org/wiki/Curvature
    curvature of space curve
    return:
        tangent vector, namely the first order derivative, CURV_N_S, the direction is the normal 
        vector direction, the magnitude is the magnitude of curvature
    """
    if s_vec is None or coor_mat_in is None:
        raise ValueError("s_vec and coor_mat cannot be None!!!")
    if len(s_vec) != len(coor_mat_in) or len(s_vec) == 0:
        raise ValueError(
            "reaction coordinate s must have a same nonzero size as cartesian coordinate!!!")
    _, n_cols = np.shape(coor_mat_in)
    coor_mat = copy.deepcopy(coor_mat_in)

    tangent_vector = [tools.calculate_1st_derivative(
        s_vec, coor_mat[:, dim_idx]) for dim_idx in range(n_cols)]
    # first_order_derivative = [tools.calculate_nth_order_derivative(
    #     s_vec, coor_mat[:, dim_idx], nth=1) for dim_idx in range(n_cols)]
    tangent_vector = np.transpose(tangent_vector)

    second_order_derivative = [tools.calculate_1st_derivative(
        s_vec, tangent_vector[:, dim_idx]) for dim_idx in range(np.shape(tangent_vector)[-1])]
    second_order_derivative = np.transpose(second_order_derivative)

    # The direction of the acceleration is the unit normal vector N(s)
    curv_N_s = copy.deepcopy(second_order_derivative)
    # return curv_N_s
    return tangent_vector, curv_N_s


def tau_tangent(n1=1, n2=2, n3=3, n4=4, XX=None):
    """
    Notice here that the tau_tangent has unit of [1/R] or says [1/X]
    /********************************************************************************
    * The purpose of this routine is to calculate the tangent vector to
    * the torsional motion of atoms N1-N2---N3-N4 in cartesian coordinates.
    * The input to this program is:
    *      -> XX is a double array containing the cartesian coordinates of
    *         the molecule arranged as (atom#,XYZ#).
    *      -> Natm is the number of atoms in the molecule.
    *      -> N1 and N4 are the indices of the terminal atoms in the
    *         dihedral coordinate.  N2 and N3 are the inner atoms.
    *      -> The connectivity is N1--N2--N3--N4.
    * The output of the program is:
    *      -> TANG is the tangent vector in cartesian coordinates of the
    *         same dimension as XX.
    * The torsion cartesian displacement formula was taken from the book
    * MOLECULAR VIBRATIONS by Wilson, Decius, and Cross pg. 61.
    **********************************************************************************/
    """
    if XX is None:
        raise ValueError("Cartesian coordiantes of atoms can not be None!!!")
    XX_local = np.ravel(copy.deepcopy(XX)).reshape(-1, 3)
    N_atom = len(XX_local)

    R12 = 0.0
    R23 = 0.0
    R32 = 0.0
    R43 = 0.0

    UV12 = np.zeros(3)
    UV23 = np.zeros(3)
    UV32 = np.zeros(3)
    UV43 = np.zeros(3)

    for i in range(len(UV12)):
        # //C and C++ start from 0
        UV12[i] = XX_local[n2-1, i]-XX_local[n1-1, i]
        UV23[i] = XX_local[n3-1, i]-XX_local[n2-1, i]
        UV43[i] = XX_local[n3-1, i]-XX_local[n4-1, i]
        R12 = R12+pow((XX_local[n2-1, i]-XX_local[n1-1, i]), 2)
        R23 = R23+pow((XX_local[n3-1, i]-XX_local[n2-1, i]), 2)
        R43 = R43+pow((XX_local[n3-1, i]-XX_local[n4-1, i]), 2)

    R12 = np.sqrt(R12)
    R23 = np.sqrt(R23)
    R43 = np.sqrt(R43)
    R32 = R23

    dot_product_1 = 0.0
    dot_product_2 = 0.0

    for i in range(len(UV12)):
        UV12[i] = UV12[i]/R12
        UV23[i] = UV23[i]/R23
        UV43[i] = UV43[i]/R43
        UV32[i] = -UV23[i]
        dot_product_1 = dot_product_1 - UV12[i] * UV23[i]
        dot_product_2 = dot_product_2 - UV32[i] * UV43[i]

    phi2 = math.acos(dot_product_1)
    phi3 = math.acos(dot_product_2)
    # // Determining the necessary crossproducts of the unit vectors
    # // needed for the dihedral tangent.
    CRP1223 = np.cross(UV12, UV23)
    CRP4332 = np.cross(UV43, UV32)

    # //We now determine the torsion tangent.
    # //We initialize the tangent array.
    tang = np.zeros((N_atom, 3))
    # print(np.shape(tang))
    # //The first atom
    fac1 = -1.0/(R12*pow(math.sin(phi2), 2))
    for i in range(np.shape(tang)[1]):
        tang[n1-1, i] = fac1*CRP1223[i]
    # //The last atom
    fac1 = -1.0/(R43*pow(math.sin(phi3), 2))
    for i in range(np.shape(tang)[1]):
        tang[n4-1, i] = fac1*CRP4332[i]
    # //The second atom
    fac1 = (R23-R12*math.cos(phi2))/(R23*R12*(pow(math.sin(phi2), 2)))
    fac2 = math.cos(phi3)/(R23*pow(math.sin(phi3), 2))
    for i in range(np.shape(tang)[1]):
        tang[n2-1, i] = fac1*CRP1223[i]+fac2*CRP4332[i]
    # //The third atom
    fac1 = (R32 - R43*math.cos(phi3))/(R32*R43*pow(math.sin(phi3), 2))
    fac2 = math.cos(phi2)/(R32*(pow(phi2, 2)))
    for i in range(np.shape(tang)[1]):
        tang[n3-1, i] = fac1*CRP4332[i]+fac2*CRP1223[i]

    # ravel it to a vector
    return np.ravel(tang)


def stretch_motion_tangent_vector(XX_in=None, n1=1, n2=2):
    """
    calculate the stretch motion tangent vector, just r1-r2, set one to be positive, one to be
    negative. This routine is for projecting out stretching motion purpose notice n1 and n2 start 
    from 1, not 0, represent the nth atom in the labelling system Gaussian uses.
    """
    if XX_in is None:
        raise ValueError("Cartesition coordinate can not be zero!!!")
    XX = np.ravel(copy.deepcopy(XX_in)).reshape(-1, 3)

    roo = 0.0
    UV = np.zeros(3)

    for i in range(len(UV)):
        UV[i] = XX[n2-1, i]-XX[n1-1, i]
        roo += pow(UV[i], 2)
    roo = np.sqrt(roo)

    for i in range(len(UV)):
        UV[i] = UV[i]/roo

    stretch = np.zeros_like(XX)

    for i in range(len(stretch[0])):
        stretch[n1-1, i] = -UV[i]
        stretch[n2-1, i] = UV[i]

    return np.ravel(stretch)


if __name__ == '__main__':
    TIME_I = time.time()

    # source file directory
    SRC_DIR = os.path.abspath(os.path.join(
        os.path.realpath(sys.argv[0]), os.pardir))
    print(SRC_DIR)

    DATA_DIR = r"/Users/invictus/Documents/data/gaussian_files/gaussian_solely/" +\
        r"case8_Carbonic_Acid_TS_freq_from_IRC/data_after"
    print(DATA_DIR)

    # notice the clips order may be very different from gaussian output file order,
    # gotta look the dihedral angles mannually
    CLIP_ANGLES = [84.70704596530202, 81.97274090939719, 79.2012254280702,
                   76.38170688831298, 73.50317171721578, 70.5547575306481,
                   67.52509750113346, 64.4031452722285, 61.17781580871703,
                   57.838477132492926, 54.374806431935994, 50.777528973867824,
                   47.039371122690746, 43.154054440163684, 39.11852723787422,
                   34.933024018528954, 30.601411458229684, 26.132658353320057,
                   21.540931547838326, 16.8460149254539, 12.074509180999012,
                   7.261753469522073, 2.4638457575723307, 178.52116921911698,
                   173.9459406907715, 169.32218160940903, 164.73286801986114,
                   160.21384914732704, 155.78855833745172, 151.47387521837817,
                   147.28159284914136, 143.21922382269926, 139.29071730229236,
                   135.49657487661887, 131.83482081911757, 128.30075724964144,
                   124.88835973528458, 121.590224767391, 118.39749388336749,
                   115.30110626373893, 112.29109107913779, 109.35742315870675,
                   106.4900963042721, 103.6785973429763, 100.91277662701586,
                   98.18222674350334, 95.4769111653637, 92.78656544837536,
                   90.1036395081854, 87.4144684392888]

    CLIP_REORDER_IDX_n2o = np.argsort(CLIP_ANGLES)[::-1]
    CLIP_REORDER_IDX_o2n = np.zeros_like(CLIP_REORDER_IDX_n2o)
    for idx, val in enumerate(CLIP_REORDER_IDX_n2o):
        CLIP_REORDER_IDX_o2n[val] = idx

    tools.rename_gauss_view_frames(os.path.join(
        DATA_DIR, "videos"), reorder_idx=CLIP_REORDER_IDX_o2n)

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
