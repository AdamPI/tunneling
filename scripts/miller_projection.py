"""
    miller projection
    https://aip.scitation.org/doi/abs/10.1063/1.438959
    Miller, William H., Nicholas C. Handy, and John E. Adams. 
    "Reaction path Hamiltonian for polyatomic molecules." 
    The Journal of chemical physics 72.1 (1980): 99-112.
"""
import sys
import time
import os
import copy
import tools
import numpy as np
import reaction_path as rp
import parse_patterns as pp


def omega_conversion_factor_atomic_unit_to_SI():
    """
    conversion factor from atomic unit to SI
    http://web.utk.edu/~rcompton/constants
    http://gaussian.com/constants/
    """
    # if we want our frequency in unit of wave numbers
    hartree_2_J = 4.35974434e-18
    # mass should be in unit of gram per mole
    amu_2_kg = 1.660538921e-27
    bohr_2_m = 0.52917721092e-10
    # in unit of cm
    speed_of_light = 2.99792458e10

    return 1.0/(2*np.pi)*np.sqrt(hartree_2_J/(bohr_2_m**2)/amu_2_kg)/speed_of_light


def angstrom_to_bohr(coor_mat):
    """
    http://gaussian.com/constants/
    1 Bohr (a0)	= 0.52917721092 Å [Mohr12, Mohr12a]
    """
    a2b_constant = 1.0/0.52917721092
    coor_mat_local = copy.deepcopy(coor_mat)

    for idx1, val in enumerate(coor_mat_local):
        for idx2, _ in enumerate(val):
            coor_mat_local[idx1, idx2] *= a2b_constant

    return coor_mat_local


def levi_civita_symbol(i, j, k):
    if ((i == 1 and j == 2 and k == 3) or (i == 2 and j == 3 and k == 1) or (i == 3 and j == 1
                                                                             and k == 2)):
        return 1
    elif ((i == 1 and j == 3 and k == 2) or (i == 3 and j == 2 and k == 1) or (i == 2 and j == 1
                                                                               and k == 3)):
        return -1
    else:
        return 0


def mom_inertia(mass=None, XX=None, flagCOM=True, flagROT=False):
    """
    * The purpose of this routine is to compute the principle moments of
    * inertia, and, if desired, rotate the geometry into the frame of the
    * principle axes.
    * The input for this routine is:
    *    -> XX contains the H2O2 geometry in an array (atom#,XYZ#).  If
    *      flagCOM is set the geometry is set relative to the center of
    *      mass.  If flagROT is set then it is set in the frame of the
    *      principle axes.
    *    -> II are the principle moments of inertia, an array (1:3).
    *    -> flagCOM=1 will set the H2O2 geometry relative to the center
    *       of mass.
    *    -> flagROT=1 will set the H2O2 geometry to the principle axes
    *       frame.
    """
    if mass is None or XX is None:
        raise ValueError(
            "mass vector and Cartesian coordinates can not be zero!!!")
    # ravel this matrix/vector first, then re-shape as a (,3) matrix
    XX_local = np.ravel(copy.deepcopy(XX)).reshape(-1, 3)

    # Put the H2O2 molecule in its enter of mass frame, if desired.
    COM = np.zeros(3)
    if flagCOM is True:
        mass_tot = 0
        for _, val in enumerate(mass):
            mass_tot += val

        for i in range(3):
            COM[i] = 0
            for j in range(len(mass)):
                COM[i] += (mass[j]/mass_tot)*XX_local[j, i]
            for j in range(len(mass)):
                XX_local[j, i] = XX_local[j, i]-COM[i]

    # /* We construct the inertial matrix taking advantage of symmetry.*/
    I = np.zeros((3, 3))
    # print(I)
    for i in range(len(mass)):
        I[0, 0] += mass[i]*(pow(XX_local[i, 1], 2)+pow(XX_local[i, 2], 2))
        I[0, 1] += (-mass[i]*XX_local[i, 0]*XX_local[i, 1])
        I[0, 2] += (-mass[i]*XX_local[i, 0]*XX_local[i, 2])
        I[1, 1] += mass[i]*(pow(XX_local[i, 0], 2)+pow(XX_local[i, 2], 2))
        I[1, 2] += (-mass[i]*XX_local[i, 1]*XX_local[i, 2])
        I[2, 2] += mass[i]*(pow(XX_local[i, 0], 2)+pow(XX_local[i, 1], 2))
    I[1, 0] = I[0, 1]
    I[2, 0] = I[0, 2]
    I[2, 1] = I[1, 2]

    inertia, eigen_vector = np.linalg.eigh(I)
    # print(inertia)
    # print(eigen_vector)
    # print(np.matrix(np.transpose(eigen_vector))
    #       * np.matrix(I)*np.matrix(eigen_vector))

    XX_local_new = copy.deepcopy(XX_local)

    # /* Now we rotate the geometry into the principle axis frame, if
    # * we set flagROT=1.*/
    if flagROT is True:
        for i in range(len(XX_local_new)):
            # this could be problematic, since the eigen vector are column vetors
            for j in range(len(XX_local_new[i])):
                XX_local_new[i, j] = np.dot(eigen_vector[:, j], XX_local[i])

    # print(XX_local_new)
    return np.ravel(inertia), np.ravel(XX_local_new)


def mass_weight_hessian_matrix(hessian_mat=None, mass_vec=None):
    """
    return the mass weighted hessian matrix
    http://www.colby.edu/chemistry/PChem/notes/NormalModesText.pdf
    """
    if hessian_mat is None:
        raise ValueError("hessian matrix is None!!!")
    if mass_vec is None:
        raise ValueError("atomic mas svector is None!!!")

    dof = len(mass_vec) * 3

    # print(hessian_mat, mass_vec)
    mass_vec_corrected = []
    for idx in range(len(mass_vec)):
        # three times, x, y, z
        mass_vec_corrected.extend(
            [mass_vec[idx], mass_vec[idx], mass_vec[idx]])

    hessian_mat_local = np.reshape(
        np.ravel(copy.deepcopy(hessian_mat)), (-1, dof))
    if len(hessian_mat_local) != 3*len(mass_vec):
        raise ValueError(
            "The size of hessian matrix and the size of atomic mass vector don't match!!!")

    for idx1 in range(len(mass_vec_corrected)):
        for idx2 in range(len(mass_vec_corrected)):
            hessian_mat_local[idx1, idx2] = hessian_mat_local[idx1, idx2] / \
                np.sqrt(mass_vec_corrected[idx1]) / \
                np.sqrt(mass_vec_corrected[idx2])

    # print(hessian_mat_local)
    # return np.matrix(hessian_mat_local)
    return np.ravel(hessian_mat_local)


def mass_weight_coordinate_matrix(XX_mat_in, mass_vec=None):
    """
    mass weight the coordinate matrix
    x_new = sqrt(m)*x_old
    """
    if XX_mat_in is None:
        raise ValueError("coordinate matrix is None!!!")
    if mass_vec is None:
        raise ValueError("atomic mas svector is None!!!")
    if np.shape(XX_mat_in)[-1] != 3*len(mass_vec):
        raise ValueError(
            "The size of coordinate matrix and the size of atomic mass vector don't match!!!")

    mass_vec_corrected = []
    for idx in range(len(mass_vec)):
        # three times, x, y, z
        mass_vec_corrected.extend(
            [mass_vec[idx], mass_vec[idx], mass_vec[idx]])

    XX_mat_local = copy.deepcopy(XX_mat_in)
    # convert to numpy array
    XX_mat_local = np.array(XX_mat_local)
    for idx in range(len(mass_vec_corrected)):
        XX_mat_local[:, idx] *= np.sqrt(mass_vec_corrected[idx])

    return XX_mat_local


def mass_weight_tau_tangent_vector(tangent_vec_in=None, mass=None, norm=True):
    """
    This tau_tangent_vector has unit of [1/R] or says unit of [1/X], where R/X
    represent distance/displacement
    one way to check is dig into the formula giving tangent vector to figure out its unit
    /****************************************************************************
    * The purpose of this routine is to mass-weight a cartesian
    * and normamlize the tangent vector
    * displace tangent vector (s_ta in the notation of Wilson, Decius, and
    * Cross).
    * The input for the routine is:
    *         -> Natm is the number of atoms in the molecule.
    *         -> mass is an array containing each of the masses of the
    *            atoms (a 1D array of size Natm).
    *         -> TANG is a double array containing the cartesian tangent
    *            vector upon input and the mass-weighted cartesian tangent
    *            vector upon output.  It is organized as (atom#,XYZ#).
    ****************************************************************************/
    """
    if tangent_vec_in is None:
        raise ValueError("Tangent vector can not be None!!!")
    if mass is None:
        raise ValueError("mass vector can not be zero!!!")

    tangent_vec = np.ravel(copy.deepcopy(tangent_vec_in)).reshape(-1, 3)
    if len(tangent_vec) != len(mass):
        raise ValueError(
            "The length of tangent vector and mass vector don't match!!!")

    Natm = len(mass)

    for i in range(Natm):
        for j in range(3):
            tangent_vec[i, j] = tangent_vec[i, j]/np.sqrt(mass[i])

    if norm is True:
        # //We finish by normalizing the tangent vector
        normy = 0.0
        for i in range(Natm):
            for j in range(3):
                normy += pow(tangent_vec[i, j], 2)
        normy = np.sqrt(normy)
        for i in range(Natm):
            for j in range(3):
                tangent_vec[i, j] /= normy

    return np.ravel(tangent_vec)


def mass_weight_X_displacement_tangent_vector(tangent_vec_in=None, mass=None, norm=True):
    """
    This tau_tangent_vector has unit of [R] or says unit of [X], where R/X
    represent distance/displacement
    one way to check is dig into the formula giving tangent vector to figure out its unit
    /****************************************************************************
    * The purpose of this routine is to mass-weight a cartesian
    * and normamlize the tangent vector
    * displace tangent vector (s_ta in the notation of Wilson, Decius, and
    * Cross).
    * The input for the routine is:
    *         -> Natm is the number of atoms in the molecule.
    *         -> mass is an array containing each of the masses of the
    *            atoms (a 1D array of size Natm).
    *         -> TANG is a double array containing the cartesian tangent
    *            vector upon input and the mass-weighted cartesian tangent
    *            vector upon output.  It is organized as (atom#,XYZ#).
    ****************************************************************************/
    """
    if tangent_vec_in is None:
        raise ValueError("Tangent vector can not be None!!!")
    if mass is None:
        raise ValueError("mass vector can not be zero!!!")

    tangent_vec = np.ravel(copy.deepcopy(tangent_vec_in)).reshape(-1, 3)
    if len(tangent_vec) != len(mass):
        raise ValueError(
            "The length of tangent vector and mass vector don't match!!!")

    Natm = len(mass)

    for i in range(Natm):
        for j in range(3):
            tangent_vec[i, j] = tangent_vec[i, j] * np.sqrt(mass[i])

    if norm is True:
        # //We finish by normalizing the tangent vector
        normy = 0.0
        for i in range(Natm):
            for j in range(3):
                normy += pow(tangent_vec[i, j], 2)
        normy = np.sqrt(normy)
        for i in range(Natm):
            for j in range(3):
                tangent_vec[i, j] /= normy

    return np.ravel(tangent_vec)


def hessian_in_au_to_wave_number(hessian_mat_in=None, mass_vec=None):
    """
    take the hessian in atomic unit, hartree/(bohr^2) do the stupid mass weighted, unit 
    conversion thing return frequency
    https://en.wikipedia.org/wiki/Diagonalizable_matrix
    Pe_k = v_k, P is the transformation matrix, P-1AP = D (P^TAP=D as well), where D is a diagonal
    matrix, unitary transformation
    return wave_num_vec, mass_weighted_atom_normal_modes, atom_normal_modes, mass_weighted_P
    mass_weighted_P is the normalized transformation matrix
    """
    if hessian_mat_in is None:
        raise ValueError("Hessian matrix is None!!!")
    if mass_vec is None:
        raise ValueError("Mass vector is None!!!")

    dof = len(mass_vec) * 3

    conversion_factor = omega_conversion_factor_atomic_unit_to_SI()
    # print(mass_vec)
    mass_weighted_hessian = np.reshape(
        mass_weight_hessian_matrix(hessian_mat_in, mass_vec), (-1, dof))

    # eigen values and eigen vectors, notice eigen vectors are unitless
    # be careful here,
    # https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.linalg.eigh.html
    eigen_values, eigen_vectors = np.linalg.eigh(mass_weighted_hessian)
    # eigen_values, eigen_vectors = np.linalg.eig(mass_weighted_hessian)

    mass_weighted_P = np.matrix(copy.deepcopy(eigen_vectors))

    # need deepcopy, otherwise the program will alternate values by reference/pointer
    eigen_vectors_1 = copy.deepcopy(eigen_vectors)
    eigen_vectors_2 = copy.deepcopy(eigen_vectors)

    wave_num_vec = np.ones_like(eigen_values)

    for idx, val in enumerate(eigen_values):
        if val < 0:
            wave_num_vec[idx] = -1 * np.sqrt(-1*val) * conversion_factor
        else:
            wave_num_vec[idx] = np.sqrt(val) * conversion_factor

    # print(wave_num_vec)

    # project eigen vector to each atom, notice eigen vector is a column vector here
    mass_weighted_atom_normal_modes = []
    for idx in range(np.shape(eigen_vectors_1)[1]):
        mass_weighted_atom_normal_modes.append(
            np.reshape(eigen_vectors_1[:, idx], (-1, 3)))
    # print(mass_weighted_atom_normal_modes[-1])

    # flatten mass vector
    mass_vec_corrected = []
    for idx in range(len(mass_vec)):
        # three times, x, y, z
        mass_vec_corrected.extend(
            [mass_vec[idx], mass_vec[idx], mass_vec[idx]])

    # original normal modes in unit of au
    atom_normal_modes = []
    for idx1 in range(np.shape(eigen_vectors_2)[1]):
        val1 = eigen_vectors_2[:, idx1]
        for idx2, _ in enumerate(val1):
            val1[idx2] /= np.sqrt(mass_vec_corrected[idx2])
        # normalize val1
        norm_2 = np.linalg.norm(val1, ord=2)
        for idx2, _ in enumerate(val1):
            val1[idx2] /= norm_2

        atom_normal_modes.append(np.reshape(val1, (-1, 3)))

    # eigen vector in unit of wavenumber
    return wave_num_vec, mass_weighted_atom_normal_modes, atom_normal_modes, mass_weighted_P


def cal_curvature_in_mass_weighted_normal_mode_basis(curv_N_s=None, h_m_in=None,
                                                     atomic_weight_vec=None, atom_name_vec=None,
                                                     tengent_vec=None, printOption=0):
    """
    curv_N_s is the curvature vector, which is mass weighted
    H_M_in is the flatten hessian matrix in atomic unit (hartree/(bohr^2)), which is NOT mass 
    weighted. Calcualte the projection matrix P(which transform vector from cartesian coordinate
    to normal mode basis. Notice the normal basis change along the reaction path, this 
    transformation matrix P shall be mass weighted), then project the mass weighted curvature 
    vector to normal mode basis.
    Atomic weight vector is in atomic unit as well, for example, the mass of Carbon(12) is 12
    """
    if curv_N_s is None or h_m_in is None or atomic_weight_vec is None:
        raise ValueError(
            "curvature vector, hessian matrix and atomic weight can not be None!!!")
    if len(curv_N_s) != len(h_m_in):
        raise ValueError(
            "curvature vector and hessian matrix should have same length!!!")
    dof = len(atomic_weight_vec) * 3
    if dof != int(np.sqrt(len(h_m_in[0]))):
        raise ValueError(
            "Shape of mass vector and Hessian matrix don't match!!!")
    print(np.shape(curv_N_s), np.shape(h_m_in))

    projected_curvature_vec = np.ones_like(curv_N_s)
    print(np.shape(projected_curvature_vec))
    for idx1, _ in enumerate(curv_N_s):
        h_m = np.ravel(copy.deepcopy(h_m_in[idx1])).reshape(dof, dof)

        wave_num_vec, mass_weighted_normal_modes, _, _ = hessian_in_au_to_wave_number(
            h_m, atomic_weight_vec)
        local_curvature_vec = curv_N_s[idx1]

        print("\nPoint {: 2d}".format(idx1+1))
        print("mode index, wave number(cm-1), X(s)'*eigen vectors, X(s)''*eigen vectors")
        for idx2, _ in enumerate(mass_weighted_normal_modes):
            local_normal_modes_in_cartesian_basis = np.ravel(
                mass_weighted_normal_modes[idx2])
            projected_curvature_vec[idx1, idx2] = np.dot(
                local_normal_modes_in_cartesian_basis, local_curvature_vec)

            print("{:>2d} {: 12.6f} {: 12.6f} {: 12.6f}".format(idx2+1, wave_num_vec[idx2], np.dot(
                local_normal_modes_in_cartesian_basis, tengent_vec[idx1]),
                projected_curvature_vec[idx1, idx2]))

        # print detailed atom specified eigen vector displacement
        if printOption == 1:
            for i2 in range(len(mass_weighted_normal_modes)):
                print(
                    "mode{: 2d}, frequency {: 12.6f}cm-1".format(i2+1, wave_num_vec[i2]))
                print("\t{:>5}, X, Y, Z, 2nd norm=(X^2+Y^2+Z^2)".format("Atom"))
                for j2 in range(len(mass_weighted_normal_modes[i2])):
                    delta_X = np.ravel(mass_weighted_normal_modes[i2][j2])
                    print("\t{:2d}{:2s}{: 8.4f}{: 8.4f}{: 8.4f}{: 8.4f}".format(
                        j2+1, atom_name_vec[j2], delta_X[0], delta_X[1], delta_X[2],
                        np.linalg.norm(delta_X, ord=2)**2))

    print(np.shape(projected_curvature_vec))
    return projected_curvature_vec


def test_normal_modes(mass_vec=None, atom_name_vec=None, XX_in=None, hessian_mat_in=None,
                      printOption=0):
    """
    !!! ALL input quantities MUST be in atomic units
    !!! It is for REACTION PATH(RP) associated quantities
    mass_vec represents atomic weight, it is a one dimensional vectorwith shape like, (6) suppose 
    there is 6 atoms. XX represents the coordinate along the reaction path, in essence, it is a 
    tensor with shape of (49, 6, 3). It is guaranteed outside that XX is the 2-3 axes are flattened
    so that XX has shape of (49, 18). Hessian_mat represents the hessian matrix, again it is 
    flattened from (49, 18, 18) to (49, 18*18)
    """
    if mass_vec is None:
        raise ValueError("Mass vector can not be None!!!")
    if atom_name_vec is None:
        raise ValueError("Atom name vector can not be None!!!")
    if XX_in is None:
        raise ValueError("Atomic cartesian coordinates can not be None!!!")
    if hessian_mat_in is None:
        raise ValueError("Hessian matrix can not be None!!!")

    # mass weight cartesian coordinate so that all quantities calculated from XX are in mass
    # weighted coordinate
    XX = mass_weight_coordinate_matrix(copy.deepcopy(XX_in), mass_vec)
    hessian_mat = copy.deepcopy(hessian_mat_in)

    rp_s_vec = rp.IRC_coordinates_to_reaction_path_s(XX)
    rp_tang_mat, curv_N_s_mat = rp.cal_tangent_curvature_along_reaction_path_s(
        rp_s_vec, XX)

    cal_curvature_in_mass_weighted_normal_mode_basis(
        curv_N_s_mat, hessian_mat, mass_vec, atom_name_vec, rp_tang_mat, printOption)

    return


def test_miller_projection(mass_vec=None, atom_name_vec=None, XX=None, hessian_mat=None,
                           max_mode_number=7, printOption=0):
    """
    !!! ALL input quantities MUST be in atomic units
    !!! It is for REACTION PATH(RP) associated quantities
    mass_vec represents atomic weight, it is a one dimensional vectorwith shape like, (6) suppose
    there is 6 atoms. XX represents the coordinate along the reaction path, in essence, it is a
    tensor with shape of (49, 6, 3). It is guaranteed outside that XX is the 2-3 axes are flattened
    so that XX has shape of (49, 18). Hessian_mat represents the hessian matrix, again it is 
    flattened from (49, 18, 18) to (49, 18*18). max_mode_number is the total number of modes we
    will project out, here it is 7 = 3 translation + 3 rotation + 1 reaction path (rp)

    /******************************************************************************
    * The purpose of this program is to project out the translation,       
    * rotation, and selected reaction path degrees of 
    * freedom from the Hessian according to the scheme of Miller, Handy,   
    * and Adams
    *     -> If flagRP = True the reaction path direction is projected out of the      
    *        hessian prior to diagonalization.                            
    ****************************************************************************/
    """
    if mass_vec is None:
        raise ValueError("Mass vector can not be None!!!")
    if atom_name_vec is None:
        raise ValueError("Atom name vector can not be None!!!")
    if XX is None:
        raise ValueError("Atomic cartesian coordinates can not be None!!!")
    if hessian_mat is None:
        raise ValueError("Hessian matrix can not be None!!!")
    if len(hessian_mat) != len(XX):
        raise ValueError(
            "Reaction path has different number of points with Hessian matrix!!!")

    NAtom = len(mass_vec)
    NPoints = len(XX)

    # mass hessian matrix
    HESS_mw = np.array([mass_weight_hessian_matrix(
        hessian_mat[idx], mass_vec) for idx in range(len(hessian_mat))])

    # get the inertia, transform the coordinates into Center of Mass (COM)
    # and rotate the geometry into principle axis frame,
    # if we set flagCOM==1 and flagROT==1
    inertia = np.zeros((NPoints, 3))
    XX_com = np.zeros_like(XX)
    for nth_p in range(NPoints):
        inertia[nth_p], XX_com[nth_p] = mom_inertia(
            mass_vec, XX[nth_p], flagCOM=True, flagROT=False)

    # mass xyz coordinates, so that curvature will be mass weighted as well
    XX_com_mw = mass_weight_coordinate_matrix(XX_com, mass_vec)

    # shall be in unit of mass weighted bohr
    # rp represents reaction path, reaction path parameter shall be treated as unitless
    # quantity, though we DEFINE it to have a physical meaning
    rp_s_vec = rp.IRC_coordinates_to_reaction_path_s(XX_com_mw)
    rp_tang_mat, rp_curv_N_mat = rp.cal_tangent_curvature_along_reaction_path_s(
        rp_s_vec, XX_com_mw)

    # normalize tangent vector
    rp_tang_mat_normed = tools.normalize_vector(rp_tang_mat)
    projected_rp_curv_N_mat = np.ones_like(rp_curv_N_mat)

    # /* We begin by determining the total mass of the system.*/
    m_tot = 0.0
    for _, val in enumerate(mass_vec):
        m_tot += val

    conversion_factor = omega_conversion_factor_atomic_unit_to_SI()

    for nth_p in range(len(XX_com_mw)):
        # /* Now the coordinates have been redefined (and re-mass weighted).
        # * We now create the projector.*/
        # /***************************************************************************************
        # *=======================3 TRANSLATION MODE VECTORS=====================*
        # /***************************************************************************************
        # * First, we make the translation mode vector.*/
        L = np.zeros((NAtom, 3, max_mode_number))
        for i in range(NAtom):
            for j in range(3):
                for k in range(3):
                    # translation, 3 degree of freedom
                    if (j == k):
                        L[i][j][k] = np.sqrt(mass_vec[i]/m_tot)
        # /***************************************************************************************
        # *=====================3 ROTATIONAL MODE VECTORS========================*
        # /***************************************************************************************
        # * Second, we make the rotational modes.*/
        XX_com_mw_nth = np.reshape(XX_com_mw[nth_p], (-1, 3))
        inertia_nth = inertia[nth_p]
        for i in range(NAtom):
            for j in range(3):
                for k in range(3, 6):
                    # rotation, 3 degree of freedom
                    L[i][j][k] = 0.0
                    for l in range(3):
                        # read the paper
                        # index of j,k,l start from 0
                        L[i][j][k] += levi_civita_symbol(
                            k-3+1, l+1, j+1)*XX_com_mw_nth[i, l]/np.sqrt(inertia_nth[k-3])

        # /***************************************************************************************
        # *====================CREATING TANGENT MODES TO==========================
        # *==========================Reaction Path================================
        # ****************************************************************************************
        rp_index = 6
        rp_tang_nth = np.reshape(rp_tang_mat_normed[nth_p], (-1, 3))
        # make sure the tangent vector along reaction path is mass weighted and normalized
        for i in range(NAtom):
            for j in range(3):
                L[i][j][rp_index] = rp_tang_nth[i, j]

        # ****************************************************************************************
        # *================CONSTRUCTION OF PROJECTOR MATRIX======================*
        # ****************************************************************************************
        # We can now construct the projector matrix.
        Pmat_4D = np.zeros((NAtom, 3, NAtom, 3))
        for i in range(NAtom):
            for j in range(3):
                for k in range(NAtom):
                    for l in range(3):
                        Pmat_4D[i][j][k][l] = 0.0
                        for m in range(max_mode_number):
                            Pmat_4D[i][j][k][l] += L[i][j][m]*L[k][l][m]

        # /* We now subtract the projector matrix from the identity matrix.
        # * This removes the undesired modes from the coordinate description of
        # * the system (i.e. set of coordinates without the translation, rotation, or
        # * gradient).*/
        Qmat = np.matrix(np.zeros((3*NAtom, 3*NAtom)))
        hess_2D = np.matrix(np.reshape(HESS_mw[nth_p], (3*NAtom, 3*NAtom)))
        index_temp = int(0)
        row_index = int(0)
        col_index = int(0)
        for i in range(NAtom):
            for j in range(3):
                for k in range(NAtom):
                    for l in range(3):
                        row_index, col_index = divmod(index_temp, NAtom*3)
                        if row_index == col_index:
                            # creat Q_matrix=1-P
                            Qmat[row_index, col_index] = 1.0 - \
                                Pmat_4D[i][j][k][l]
                            # Qmat[row_index, col_index]=1.0-Pmat_4D[k][l][i][j]
                        else:
                            Qmat[row_index, col_index] = -Pmat_4D[i][j][k][l]
                            # Qmat[row_index, col_index]=-Pmat_4D[k][l][i][j]
                        index_temp += 1

        # Creat the projected_hess
        # K_p=(1-P)*K*(1-P)
        K_P = Qmat*hess_2D*Qmat

        # Diagonalize
        # eigen values and eigen vectors, notice eigen vectors are unitless
        # be careful here,
        # https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.linalg.eigh.html
        eigen_values, eigen_vectors = np.linalg.eigh(K_P)
        # eigen_values, eigen_vectors = np.linalg.eig(K_P)

        eigen_vectors_1 = copy.deepcopy(eigen_vectors)

        wave_num_vec = np.ones_like(eigen_values)
        for idx, val in enumerate(eigen_values):
            if val < 0:
                wave_num_vec[idx] = -1 * np.sqrt(-1*val) * conversion_factor
            else:
                wave_num_vec[idx] = np.sqrt(val) * conversion_factor

        # project eigen vector to each atom, notice eigen vector is a column vector here
        mass_weighted_normal_modes = []
        for idx in range(np.shape(eigen_vectors_1)[1]):
            mass_weighted_normal_modes.append(
                np.reshape(eigen_vectors_1[:, idx], (-1, 3)))

        local_curvature_vec = rp_curv_N_mat[nth_p]

        print("\nPoint {: 2d}".format(nth_p+1))
        print("mode index, wave number(cm-1), X(s)'*eigen vectors, X(s)''*eigen vectors")
        for idx2, _ in enumerate(mass_weighted_normal_modes):
            local_normal_modes_in_cartesian_basis = np.ravel(
                mass_weighted_normal_modes[idx2])
            projected_rp_curv_N_mat[nth_p, idx2] = np.dot(
                local_normal_modes_in_cartesian_basis, local_curvature_vec)

            print("{:>2d} {: 12.6f} {: 12.6f} {: 12.6f}".format(idx2+1, wave_num_vec[idx2], np.dot(
                local_normal_modes_in_cartesian_basis, rp_tang_mat_normed[nth_p]),
                projected_rp_curv_N_mat[nth_p, idx2]))

        # print detailed atom specified eigen vector displacement
        if printOption == 1:
            for i2 in range(len(mass_weighted_normal_modes)):
                print(
                    "mode{: 2d}, frequency {: 12.6f}cm-1".format(i2+1, wave_num_vec[i2]))
                print("\t{:>5}, X, Y, Z, 2nd norm=(X^2+Y^2+Z^2)".format("Atom"))
                for j2 in range(len(mass_weighted_normal_modes[i2])):
                    delta_X = np.ravel(mass_weighted_normal_modes[i2][j2])
                    print("\t{:2d}{:2s}{: 8.4f}{: 8.4f}{: 8.4f}{: 8.4f}".format(
                        j2+1, atom_name_vec[j2], delta_X[0], delta_X[1], delta_X[2],
                        np.linalg.norm(delta_X, ord=2)**2))

    return


if __name__ == '__main__':
    TIME_I = time.time()

    # source file directory
    SRC_DIR = os.path.abspath(os.path.join(
        os.path.realpath(sys.argv[0]), os.pardir))
    print(SRC_DIR)

    # data directory
    DATA_DIR = r"E:\Documents\gaussian_files\gaussian_solely" +\
        r"\case9_Carbonic_Acid_Monomethyl_Ester_IRC\data_after"
    print(DATA_DIR)

    # read global settings
    # G_S = global_settings.get_setting(DATA_DIR)

    # parse_optimized_coordinates_xyz(DATA_DIR, G_S)
    # parse_hessian_matrix(DATA_DIR, G_S)
    # parse_forces_gradients(DATA_DIR, G_S)
    # parse_total_energy_fchk(DATA_DIR)
    # parse_IRC_reaction_coordinate_input_log(DATA_DIR)
    # parse_number_atom_atomic_weight_input_log(DATA_DIR)
    # parse_atom_vector_input_log(DATA_DIR)

    # O2, C1, O4, H5
    COOR_MAT_FOR_DIHEDRAL_ANGLE = pp.parse_IRC_reaction_coordinate_input_log(
        DATA_DIR, reorder_idx=None)
    _, OLD_2_NEW, DI_ANGLE, SORTED_DI_ANGLE = \
        rp.cal_dihedral_angle_from_IRC_path(DATA_DIR, COOR_MAT_FOR_DIHEDRAL_ANGLE,
                                            [2, 1, 4, 5])
    print(DI_ANGLE)
    print(OLD_2_NEW)

    _, A_N, A_W = pp.parse_atom_vector_input_log(DATA_DIR)

    XX_IN_ANGSTROM = pp.parse_IRC_reaction_coordinate_input_log(
        DATA_DIR, reorder_idx=OLD_2_NEW)
    # angstrom to bohr, unit conversion
    XX_IN_AU = angstrom_to_bohr(XX_IN_ANGSTROM)
    _, _, DI_ANGLE_NEW, _ = rp.cal_dihedral_angle_from_IRC_path(
        DATA_DIR, XX_IN_AU, [2, 1, 4, 5])
    print("Dihedral angle along reaction path:")
    print(DI_ANGLE_NEW)

    HESS_IN_AU = pp.parse_IRC_hessian_matrix_input_log(
        DATA_DIR, None, None, True, reorder_idx=OLD_2_NEW)

    test_normal_modes(A_W, A_N, XX_IN_AU, HESS_IN_AU, printOption=0)
    # test_miller_projection(A_W, A_N, XX_IN_AU, HESS_IN_AU, printOption=0)

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
