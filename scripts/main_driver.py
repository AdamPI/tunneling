"""
main driver
"""

import os
import sys
import time

import update_settings as us
import job_drivers
import global_settings
import parse_patterns

if __name__ == '__main__':
    TIME_I = time.time()

    # source file directory
    SRC_DIR = os.path.abspath(os.path.join(
        os.path.realpath(sys.argv[0]), os.pardir))
    print(SRC_DIR)
    # data directory
    DATA_DIR = os.path.abspath(os.path.join(os.path.realpath(
        sys.argv[0]), os.pardir, os.pardir, "gaussian", "data"))
    print(DATA_DIR)

    # read global settings
    G_S = global_settings.get_setting(DATA_DIR)

    # # quick clean up
    # job_drivers.quick_clean_up(DATA_DIR, G_S)

    # run gaussian job once
    job_drivers.gaussian_job_once(DATA_DIR, G_S)

    # # freq calculation from IRC path results
    # job_drivers.freq_calculation_from_IRC_path(DATA_DIR, G_S, path_point0=0, path_point1=48)

    # # form check point file
    # job_drivers.form_check_point_file(
    #     DATA_DIR, str(G_S["checkpoint_file_name"]) + ".chk")

    # # extract coordinates from .fchk file
    # parse_patterns.parse_optimized_coordinates_xyz(DATA_DIR, G_S)

    # # extract hessian matrix from .fchk file
    # parse_patterns.parse_hessian_matrix(DATA_DIR, G_S)

    # # extract atomic weight
    # parse_patterns.parse_atomic_weight(DATA_DIR, G_S)

    # # extract forces
    # parse_patterns.parse_forces_gradients(DATA_DIR, G_S)

    # # extract total energy
    # parse_patterns.parse_total_energy_fchk(DATA_DIR)

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
