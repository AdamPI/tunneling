"""
Job drivers
"""
import subprocess
import os
import sys
import time
import numpy as np
import update_settings as us
import global_settings
import parse_patterns
import tools


def quick_clean_up(data_dir, g_s):
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    input_log_fn = os.path.join(data_dir, "input.log")
    try:
        os.remove(input_log_fn)
    except OSError:
        pass

    check_chk_fn = os.path.join(data_dir,
                                str(g_s["checkpoint_file_name"]) + ".chk")
    try:
        os.remove(check_chk_fn)
    except OSError:
        pass

    check_fchk_fn = os.path.join(data_dir,
                                 str(g_s["checkpoint_file_name"]) + ".fchk")
    try:
        os.remove(check_fchk_fn)
    except OSError:
        pass

    return


def gaussian_job_once(data_dir, g_s, local_geometry=None):
    """
    Run gaussian job once
    """
    os.chdir(data_dir)
    # quick_clean_up(data_dir, g_s)
    us.update_gaussian_job_setting(data_dir, g_s, local_geometry)
    gaussian_run(data_dir)
    # form_check_point_file(data_dir, str(g_s["checkpoint_file_name"]) + ".chk")


def freq_calculation_from_IRC_path(data_dir, g_s, path_point0=0, path_point1=0):
    """
    base on IRC calculation result, do freq calculation at each point
    point_start, point_end are integers
    """
    _, atom_vector, _ = parse_patterns.parse_atom_vector_input_log(
        data_dir)
    IRC_path_xyz = parse_patterns.parse_IRC_reaction_coordinate_input_log(
        data_dir)

    for p_p in range(path_point0, path_point1+1):
        path_dir = os.path.join(data_dir, "path_" + str(p_p))
        # create directory if not exists
        if not os.path.exists(path_dir):
            os.makedirs(path_dir)
        if p_p < 0 or p_p > len(IRC_path_xyz):
            raise ValueError("Invalid path point number!!!")

        # prepare local geometry
        local_geometry = []
        local_xyz_1D = IRC_path_xyz[int(p_p)]
        local_xyz_3D = np.reshape(local_xyz_1D, (-1, 3))

        for idx in range(np.shape(local_xyz_3D)[0]):
            entry = {}
            name_idx = idx % len(atom_vector)
            entry["atom"] = atom_vector[name_idx]
            entry["xyz"] = list(local_xyz_3D[idx])
            local_geometry.append(entry)

        gaussian_job_once(path_dir, g_s, local_geometry)


def form_check_point_file(data_dir, chk_fn):
    """
    form check point file so that we can read
    """
    os.chdir(data_dir)
    print(chk_fn)
    cmd = "formchk " + chk_fn
    print(cmd)
    os.system(cmd)


def gaussian_run(data_dir):
    """
    src_dir, source file (C++ exetutable file)
    make run
    """
    os.chdir(data_dir)
    print(data_dir)
    cmd = "g16 input.inp"
    print(cmd)
    os.system(cmd)


if __name__ == '__main__':
    TIME_I = time.time()

    # source file directory
    SRC_DIR = os.path.abspath(os.path.join(
        os.path.realpath(sys.argv[0]), os.pardir))
    print(SRC_DIR)
    # data directory
    DATA_DIR = r"E:\Documents\gaussian_files\gaussian_solely\case5_Carbonic_Acid_TS_IRC\data_after"
    print(DATA_DIR)

    # read global settings
    G_S = global_settings.get_setting(DATA_DIR)

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
