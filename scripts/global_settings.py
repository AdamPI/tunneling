"""
global settings
"""
import sys
import os
import time
import copy
from collections import OrderedDict, defaultdict


def get_setting(data_dir):
    """
    return global settings
    """
    settings = {}
    try:
        sys.path.append(data_dir)
        # pylint: disable=locally-disabled, import-error
        import local_settings
        return local_settings.get_local_settings()
    except IOError:
        return settings


def get_atom_vector(g_s):
    """
    return a vector of atom
    """
    settings = copy.deepcopy(g_s)
    atom_vec = []
    if "geometry" not in settings:
        raise ValueError("could not find Geometry!!!")
    for _, val in enumerate(settings["geometry"]):
        if "atom" not in val:
            raise ValueError("bad entry, atom not found!!!")
        # print(val["atom"])
        atom_vec.append(val["atom"])

    return atom_vec


if __name__ == '__main__':
    TIME_I = time.time()

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
