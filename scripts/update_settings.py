"""
update settings.json
"""

import os
import sys
import time
import copy
from shutil import copy2
import re
import read_write_configuration as rwc
import global_settings


def update_gaussian_job_setting(data_dir, g_s, local_geometry=None):
    """
    update input.inp, the basic information that's will not change for this system
    """
    # there will always be a current setting
    fn1 = os.path.join(data_dir, "input.inp")

    settings = copy.deepcopy(g_s)
    print(settings)

    # update system environment
    if "scratch_dir" in settings and settings["scratch_dir"] is not None:
        scratchdir = str(settings["scratch_dir"])
        os.system("export GAUSS_SCRDIR=" + str(scratchdir))

    with open(fn1, 'w') as f_handler:
        # number of processor
        n_processor = 1
        if "number_of_processors" in settings:
            if settings["number_of_processors"] is not None \
                    and int(settings["number_of_processors"]) > 0:
                n_processor = int(settings["number_of_processors"])
        f_handler.write("%nprocshared=" + str(n_processor) + '\n')

        # check point file name
        cp_fn = "chk_point_file"
        if "checkpoint_file_name" in settings:
            if settings["checkpoint_file_name"] is not None \
                    and len(str(settings["checkpoint_file_name"])) > 0:
                cp_fn = str(settings["checkpoint_file_name"])
        f_handler.write("%chk=" + str(cp_fn) + ".chk" + '\n')

        # amount of memory
        amount_mem = "1GB"
        if ("amount_of_memory" in settings
                and settings["amount_of_memory"] is not None
                and len(str(settings["amount_of_memory"])) >= 3
                and settings["amount_of_memory"][-2::] == 'GB'
                and int(settings["amount_of_memory"][0:-2]) >= 1):
            amount_mem = str(int(settings["amount_of_memory"][0:-2])) + 'GB'

        f_handler.write("%mem=" + str(amount_mem) + '\n')

        # method, basis, optimization options, symmetry and others
        mbos = "#P B3LYP/3-21G* opt=(calcall,tight,noeigentest,TS)  nosymm"
        if "method" in settings and settings["method"] is not None:
            mbos = "#P " + str(settings["method"])
        else:
            raise ValueError("could not find Method!!!")
        if "basis" in settings and settings["basis"] is not None:
            mbos += "/" + str(settings["basis"])
        else:
            raise ValueError("could not find Basis!!!")

        if "energy" in settings and settings["energy"] is not None:
            mbos += ""
        elif "optimization_options" in settings \
                and settings["optimization_options"] is not None:
            mbos += " " + str(settings["optimization_options"])
        elif "frequency_options" in settings and settings["frequency_options"] is not None:
            mbos += " " + str(settings["frequency_options"])
        elif "optimization_and_frequency_options" in settings \
                and settings["optimization_and_frequency_options"] is not None:
            mbos += " " + str(settings["optimization_and_frequency_options"])
        elif "intrinsic_reaction_coordinate" in settings \
                and settings["intrinsic_reaction_coordinate"] is not None:
            mbos += " " + str(settings["intrinsic_reaction_coordinate"])
        else:
            raise ValueError(
                "could not find energy, optimization, frequency, or irc options!!!")
        if "symmetry" in settings:
            if settings["symmetry"] is None:
                symmetry = "nosymm"
            else:
                symmetry = str(settings["symmetry"])
            mbos += " " + symmetry
        if "guess" in settings:
            if not settings["guess"] is None:
                guess = str(settings["guess"])
                mbos += " " + "guess=" + guess
        if "geom" in settings:
            if not settings["geom"] is None:
                geom = str(settings["geom"])
                mbos += " " + "geom=" + geom
        if "any_other_options" in settings:
            if settings["any_other_options"] is not None:
                if str(settings["any_other_options"]) != "":
                    mbos += " " + str(settings["any_other_options"])
        f_handler.write(mbos + '\n')

        # empty line
        f_handler.write('\n')

        # name line
        title_line = "Title Card Required"
        if "title" in settings and settings["title"] is not None:
            if len(str(settings["title"])) >= 1:
                title_line = str(settings["title"])
        f_handler.write(title_line + '\n')

        # empty line
        f_handler.write('\n')

        # charge and multiplicity
        charge = "0"
        multiplicity = "1"
        if "charge" in settings and settings["charge"] is not None:
            charge = str(settings["charge"])
        if "multiplicity" in settings and settings["multiplicity"] is not None:
            multiplicity = str(settings["multiplicity"])
        f_handler.write(charge + " " + multiplicity + '\n')

        # if "Geom=checkpoint", "geom=checkpoint", "Geom=check", "Geom=Check",
        # "geom=check" or "geom=check", etc. option is used, skip molecule geometry
        found = re.search(r"[Gg]eom=(?:all)*[Cc]heck(point)*", mbos)
        if found:
            print("read geomotry from checkfile ...")
            if not os.path.isfile(os.path.join(data_dir, str(cp_fn) + ".chk")):
                raise ValueError(
                    "could not find checkfile with name " + str(cp_fn) + ".chk" + "!!!")
        # user input geometry
        else:
            # molecule geometry
            if "geometry" not in settings and local_geometry is None:
                raise ValueError("could not find Geometry!!!")
            if local_geometry is not None:
                geometry_here = copy.deepcopy(local_geometry)
            elif "geometry" in settings:
                geometry_here = copy.deepcopy(settings["geometry"])

            for _, val in enumerate(geometry_here):
                entry = ""
                if "atom" not in val:
                    raise ValueError("bad entry, atom not found!!!")
                print(val["atom"])
                entry += val["atom"]
                if "xyz" not in val:
                    raise ValueError(
                        "bad entry, xyz coordinate not found, " +
                        "only support cartesian coordinates!!!")
                print(val["xyz"])
                for _, val2 in enumerate(val["xyz"]):
                    entry += " " + "{0:1.8E}".format(float(val2))
                entry += '\n'
                f_handler.write(entry)

        # empty line
        f_handler.write('\n')

        # explicitly include basis set and pseudo-potential
        # implies the explicit declaration of an external pseudopotential or Effective Core
        # Potential (ECP)
        # https://joaquinbarroso.com/2011/11/02/gen-keyword-gaussian/
        # https://bse.pnl.gov/bse/portal
        if "pseudo=read" in mbos \
                or "/gen" in mbos:
            print("read both basis set and pseudo-potential from file ...")
            if "external_basis_set_pseudopotential" not in settings \
                    or settings["external_basis_set_pseudopotential"] is None \
                    or not os.path.isfile(
                        os.path.join(data_dir, settings["external_basis_set_pseudopotential"])):
                raise ValueError(
                    "could not find external_basis_set_pseudopotential file + !!!")
            # find the file, append it to input.inp
            # https://stackoverflow.com/questions/3277503/in-python-how-do-i-read-a-file-line-by-line-into-a-list
            lines = [line.rstrip('\n') for line in open(os.path.join(
                data_dir, settings["external_basis_set_pseudopotential"]))]
            for line in lines:
                f_handler.write(line)
                # empty line
                f_handler.write('\n')

        # empty line
        f_handler.write('\n')
    return


if __name__ == '__main__':
    TIME_I = time.time()

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
