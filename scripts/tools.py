"""
tools, for example routine helping making figures
"""
import os
import time
import sys
import re
import shutil

import matplotlib
matplotlib.use('Agg')
from matplotlib import pylab as plt
from matplotlib.lines import Line2D

import numpy as np
import scipy.interpolate
import glob


def get_colors_markers_linestyles():
    """
    return colors
    markers
    linestyles
    """
    markers_tmp = []
    for m_k in Line2D.markers:
        try:
            if len(m_k) == 1 and m_k != ' ':
                markers_tmp.append(m_k)
        except TypeError:
            pass

    markers_tmp = markers_tmp + [
        r'$\lambda$',
        r'$\bowtie$',
        r'$\circlearrowleft$',
        r'$\clubsuit$',
        r'$\checkmark$']

    markers = markers_tmp[2::]
    markers.append(markers_tmp[0])
    markers.append(markers_tmp[1])

    colors = ('b', 'g', 'k', 'c', 'm', 'y', 'r')
    linestyles = Line2D.lineStyles.keys()

    return colors, markers, linestyles


def make_figure_template(data_dir):
    """
    make_figure_template
    """
    colors, markers, _ = get_colors_markers_linestyles()
    # x axis file name
    f_n_x = "fname_x.csv"
    # y axis file name
    f_n_y = "fname_y.csv"
    # figure name
    fig_name = "test.jpg"

    data_x = np.loadtxt(os.path.join(data_dir, f_n_x),
                        dtype=float, delimiter=",")
    data_y = np.loadtxt(os.path.join(data_dir, f_n_y),
                        dtype=float, delimiter=",")
    # specify label for lines
    labels = ["line" + str(i + 1) for i in range(len(data_y))]

    delta_n = int(len(data_x) / 25)
    if delta_n is 0:
        delta_n = 1

    fig, a_x = plt.subplots(1, 1, sharex=True, sharey=False)
    for idx, _ in enumerate(data_y):
        a_x.plot(data_x[::delta_n], data_y[idx, ::delta_n],
                 color=colors[idx], marker=markers[idx], label=labels[idx])

    leg = a_x.legend(loc=0, fancybox=True, prop={'size': 10.0})
    leg.get_frame().set_alpha(0.7)

    a_x.set_xlim([data_x[0], data_x[-1]])
    a_x.grid()

    a_x.set_xlabel("1000/T(K$^{-1}$)")
    a_x.set_ylabel("k(cm$^{3}$ molecule$^{-1}$s$^{-1}$)")
    a_x.set_title("O$_2$ + npropyl")

    fig.tight_layout()
    fig.savefig(os.path.join(data_dir, fig_name), dpi=500)
    plt.close()


def calculate_bond_length(v1=None, v2=None):
    """
    calculate bond length given two [x y z] vectors
    """
    if v1 is None or v2 is None or len(v1) != len(v2):
        raise ValueError("[x y z] shape vector is required!!!")

    return np.sqrt(np.dot(v1, v2))


def calculate_bond_angle(v1=None, v2=None, v3=None):
    """
    calcualte bond angle given cartesian coordinates of three atoms
    """
    if v1 is None or\
            v2 is None or\
            v3 is None or\
            len(v1) != len(v2) or\
            len(v1) != len(v3):
        raise ValueError("[x y z] shape vector is required!!!")
    u1 = np.subtract(v1, v2)
    u2 = np.subtract(v3, v2)
    print(u1, u2)
    print(np.dot(u1, u2))
    norm1 = np.dot(u1, u1)
    norm2 = np.dot(u2, u2)
    print(norm1, norm2)

    if norm1 == 0:
        raise ValueError("v1 and v2 are the same, no bond angle!!!")
    if norm2 == 0:
        raise ValueError("v2 and v3 are the same, no bond angle!!!")

    cos_phi = np.dot(u1, u2)/(norm1*norm2)
    angle_finale = np.arccos(cos_phi)
    print("The angle in unit of radian is :\t", angle_finale)
    print("The angle in unit of degree is :\t", angle_finale*180/np.pi)
    return angle_finale*180/np.pi


def calculate_dihedral_angle(v1=None, v2=None, v3=None, v4=None):
    """
    calculate dihedral angle given cartesian coordinates of four atoms
    https://math.stackexchange.com/questions/47059/how-do-i-calculate-a-dihedral-angle-given-cartesian-coordinates
    It seems a single complete answer has not yet been given. Here is what I think is the most straightforward and numerically stable solution, expressed in a way that is easy to implement. It overlaps a lot with Vhailor's and Jyrki's answers, so I've marked it community wiki.
    Given the coordinates of the four points, obtain the vectors b1, b2, and b3 by vector subtraction.
    Let me use the nonstandard notation ⟨v⟩ to denote v/∥v∥, the unit vector in the direction of the vector v. Compute n1=⟨b1×b2⟩ and n2=⟨b2×b3⟩, the normal vectors to the planes containing b1 and b2, and b2 and b3 respectively. The angle we seek is the same as the angle between n1 and n2.
    The three vectors n1, ⟨b2⟩, and m1:=n1×⟨b2⟩ form an orthonormal frame. Compute the coordinates of n2 in this frame: x=n1⋅n2 and y=m1⋅n2. (You don't need to compute ⟨b2⟩⋅n2 as it should always be zero.)
    The dihedral angle, with the correct sign, is atan2(y,x).
    (The reason I recommend the two-argument atan2 function to the traditional cos−1 in this case is both because it naturally produces an angle over a range of 2π, and because cos−1 is poorly conditioned when the angle is close to 0 or ±π.)
    """
    b1 = np.subtract(v2, v1)
    b2 = np.subtract(v3, v2)
    b3 = np.subtract(v4, v3)
    # print(b1, b2, b3)

    n1_unnormalized = np.cross(b1, b2)
    n1_power2 = np.dot(n1_unnormalized, n1_unnormalized)
    if n1_power2 == 0:
        raise ValueError("v2-v1 and v3-v2 are parallel, no dihedral angle!!!")
    n1 = n1_unnormalized / (np.sqrt(n1_power2))
    # print(n1)

    n2_unnormalized = np.cross(b2, b3)
    n2_power2 = np.dot(n2_unnormalized, n2_unnormalized)
    if n2_power2 == 0:
        raise ValueError("v3-v2 and v4-v3 are parallel, no dihedral angle!!!")
    n2 = n2_unnormalized / (np.sqrt(n2_power2))
    # print(n2)

    # calculate the angle between two normal vectors
    x_vec = np.dot(n1, n2)
    m1 = np.cross(n1, b2)
    y_vec = np.dot(m1, n2)

    angle_finale = np.arctan2(y_vec, x_vec)
    # print("The angle in unit of radian is :\t", angle_finale)
    # print("The angle in unit of degree is :\t", angle_finale*180/np.pi)

    return angle_finale*180/np.pi


def calculate_1st_derivative(x_vec=None, y_vec=None):
    """
    given x vector and y vector
    return dy/dx
    """
    if x_vec is None or y_vec is None:
        raise ValueError("Input vectors cannot be None!!!")
    if len(x_vec) != len(y_vec) or len(x_vec) == 0:
        raise ValueError("Input vectors must have same non-zero length!!!")
    return np.gradient(y_vec, x_vec, edge_order=2)


def calculate_nth_order_derivative(x_vec=None, y_vec=None, nth=2):
    """
    given x vector and y vector
    return dy''/dx''
    https://stackoverflow.com/questions/23419193/second-order-gradient-in-numpy
    """
    if x_vec is None or y_vec is None:
        raise ValueError("Input vectors cannot be None!!!")
    if len(x_vec) != len(y_vec) or len(x_vec) == 0:
        raise ValueError("Input vectors must have same non-zero length!!!")
    # no smoothing, 3rd order spline
    spl = scipy.interpolate.splrep(x_vec, y_vec, k=3)
    # use those knots to get second derivative
    ddy = scipy.interpolate.splev(x_vec, spl, der=nth)
    return ddy


def normalize_vector(v_in=None):
    """
    v_in can ve either a 1D vector or 2D array
    return numpy array with the same shape
    """
    if v_in is None:
        raise ValueError("Input vector/matrix can not be None!!!")
    dim = len(np.shape(v_in))

    v_out = np.zeros_like(v_in)

    if dim == 1:
        norm2 = np.linalg.norm(v_in, ord=2)
        for idx, _ in enumerate(v_out):
            v_out[idx] = v_in[idx] / norm2
    elif dim == 2:
        for idx1, _ in enumerate(v_out):
            norm2 = np.linalg.norm(v_in[idx1], ord=2)
            for idx2, _ in enumerate(v_out[idx1]):
                v_out[idx1][idx2] = v_in[idx1][idx2] / norm2

    return v_out


def rename_gauss_view_frames(data_dir=None, reorder_idx=None, prefix="sorted"):
    """
    rename gauss view clips based on the reorder_idx array
    """
    if data_dir is None:
        raise ValueError("data directory can not be None!!!")
    if reorder_idx is None:
        raise ValueError("reorder index list can not be None!!!")
    if not os.path.isdir(data_dir):
        raise OSError("Path can not be found!!!")
    print(reorder_idx)
    # https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
    pattern_s = r"([A-Za-z]+)(\d+)"
    for fn in glob.glob(os.path.join(data_dir, "Clip*.png")):
        src_name = os.path.basename(fn)
        # print(src_name)
        m = re.search(pattern_s, src_name)
        if m:
            print(m.groups())
            # print(m.groups()[0])
            # print(m.groups()[1])
            # print(int(m.groups()[1]))
            old_int = int(m.groups()[1])
            # make the index start from 1 instead of 0
            new_int = int(reorder_idx[old_int]) + 1

            sorted_name = "Sorted" + \
                m.groups()[0] + "{:0{prec}d}".format(new_int,
                                                     prec=len(m.groups()[1])) + ".png"
            print(sorted_name)
            shutil.copy(os.path.join(data_dir, src_name),
                        os.path.join(data_dir, sorted_name))

    return


if __name__ == '__main__':
    TIME_I = time.time()

    # source file directory
    SRC_DIR = os.path.abspath(os.path.join(
        os.path.realpath(sys.argv[0]), os.pardir))
    print(SRC_DIR)

    # print(calculate_bond_length([1, 2, 3], [1, 2, 3]))
    # calculate_bond_angle([1, 2, 3], [3, 5, 7], [4, 5, 1])
    # calculate_dihedral_angle([1, 2, 3], [3, 5, 6], [2, 7, 3], [1, 5, 6])

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
