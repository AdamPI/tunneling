# Tunneling effect
General tunneling and deep tunneling

* Bao, J. L., & Truhlar, D. G. (2017). Variational transition state theory: theoretical framework and recent developments. Chemical Society Reviews, 46(24), 7548-7596.

* Meisner, J., & K�stner, J. (2016). Atom tunneling in chemistry. Angewandte Chemie International Edition, 55(18), 5400-5413.

* Beyer, A. N., Richardson, J. O., Knowles, P. J., Rommel, J., & Althorpe, S. C. (2016). Quantum Tunneling Rates of Gas-Phase Reactions from On-the-Fly Instanton Calculations. The journal of physical chemistry letters, 7(21), 4374-4379.

* Gupta, R., Li, X. X., Cho, K. B., Guo, M., Lee, Y. M., Wang, Y., ... & Nam, W. (2017). Tunneling Effect That Changes the Reaction Pathway from Epoxidation to Hydroxylation in the Oxidation of Cyclohexene by a Compound I Model of Cytochrome P450. The journal of physical chemistry letters, 8(7), 1557-1561.

* Hines, D. A., Forrest, R. P., Corcelli, S. A., & Kamat, P. V. (2015). Predicting the Rate Constant of Electron Tunneling Reactions at the CdSe�TiO2 Interface. The Journal of Physical Chemistry B, 119(24), 7439-7446.

* Schreiner, P. R. (2017). Tunneling Control of Chemical Reactions: The Third Reactivity Paradigm. Journal of the American Chemical Society, 139(43), 15276-15283.

* Pollak, E. (2017). Quantum Tunneling: The Longer the Path, the Less Time it Takes. The journal of physical chemistry letters, 8(2), 352-356.

* Suleimanov, Y. V., Aoiz, F. J., & Guo, H. (2016). Chemical reaction rate coefficients from ring polymer molecular dynamics: theory and practical applications. The Journal of Physical Chemistry A, 120(43), 8488-8502.

* Eckle, P., Pfeiffer, A. N., Cirelli, C., Staudte, A., D�rner, R., Muller, H. G., ... & Keller, U. (2008). Attosecond ionization and tunneling delay time measurements in helium. science, 322(5907), 1525-1529.

* Shafir, D., Soifer, H., Bruner, B. D., Dagan, M., Mairesse, Y., Patchkovskii, S., ... & Dudovich, N. (2012). Resolving the time when an electron exits a tunnelling barrier. Nature, 485(7398), 343.
